export interface ServerLogs {
    group: string;
    activity: string;
    item: string;
    itemName: string;
}