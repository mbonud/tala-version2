export interface Tokens {
	userId?: string;
	accessToken: string;
	accessExpires: number;
	tokenType: string;
	refreshToken: string;
	refreshExpires: number;
}
