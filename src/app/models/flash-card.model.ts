export interface CardList {
	cardList: string[];
	nextSet: string[];
	cards: FlashCard[];
}

export interface CardsNext {
	next: string;
	prev: string;
	cards: FlashCard;
}

export interface FlashCard {
	cardId: string;
	cardName: string;
	cardThumbnail: string;
	cardDescription: string;	
	cardUrl: string;
	cardType: string;
	groupName: string;
	groupId: string;
	rankId: [];
	cardFileName: string;
	cardFileSize: string;
	cards: FlashCard[];
	url: string;
	no: number;
}

export interface FlashCardBody {	                               	
	contentType: string;
	needPayment: string;
	// cardId: string;
	cardName: string;
	cardThumbnail: string;
	cardDescription: string;	
	// cardUrl: string;
	cardType: string;
	// groupName: string;
	groupId: string;
	rankId: string[];
	cardFileName: string;
	cardFileSize: string;
}

export interface Cards{
	cardId: string;
	cardName: string;
	cardThumbnail: string;
	cardDescription: string;	
	cardType: string;
	groupName: string;
	cardUrl: string;
	groupId: string;
	rankId: [];
	cardFileName: string;
	cardFileSize: string;
	cards: FlashCard[];
	url: string;
	no: number;
}

export interface FlashCards{
	cardId: string;
	cardName: string;
	cardThumbnail: string;
	cardDescription: string;	
	cardType: string;
	groupName: string;
	cardUrl: string;
	groupId: string;
	rankId: [];
	cardFileName: string;
	cardFileSize: string;
}	