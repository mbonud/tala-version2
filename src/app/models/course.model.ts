export interface CourseList{
	courseList: string[];
	nextSet: string[];
	courses: Course[];
}

export interface CourseNext{
	next: string;
	prev: string;
	courses: Course;
}

export interface Course {
	courseName: string;
	courseDescription: string;
	rankId: [];
	courseType: string;
	groupId: string;	
	courseThumbnail: string;
	courseFileName: string;			
	courseFileSize: string;
	contentType: string;
	needPayment: string;
	preview: string;	
	groupName: string;	
	courseId: string;		

	courseUrl: string;	
	
  	courses: Course[];
	url: string;
	no: string;	
	chapters: string;
	
}

export interface CourseBody {
	courseName: string;
	courseDescription: string;
	rankId: string[];
	courseType: string;
	groupId: string;	
	courseThumbnail: string;
	courseFileName: string;			
	courseFileSize: string;	
	contentType: string;
	needPayment: string;
	preview: string;
	// groupName: string;	
	// courseId: string;
}

export interface CourseBodyUpdate {
	courseName: string;
	courseDescription: string;
	rankId: string[];
	courseType: string;
	groupId: string;	
	courseThumbnail: string;
	courseFileName: string;			
	courseFileSize: string;	
	contentType: string;
	needPayment: string;
	preview: string;
	// groupName: string;	
	courseId: string;
}

export interface Courses {
	courseDescription: string;
	courseFileName: string;
	courseId: string;		
	courseName: string;	
	courseThumbnail: string;
	courseUrl: string;
	courseType: string;
	groupName: string;
	groupId: string;
	rankId: [];	
	env: string;
	createdAt: string;
	createdDate: string;
	createdTime: string;
	updatedAt: string;
	updatedDate: string;
	updatedTime: string;
	millis: number;
}

export interface Coursess{
	env: string;
	createdAt: string;
	createdDate: string;
	createdTime: string;
	updatedAt: string;
	updatedDate: string;
	updatedTime: string;
	millis: number;
}