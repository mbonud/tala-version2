
export interface PrincpalList{
	groups: Principal[];
	prev: string;
	next: string;
}

export interface Principal{
    no:number;
    groupName: string;
    groupAddress: string;
    groupId: string;
    groupCode: string;
    groupEmail: string;
    contactPerson: string;
    contactNumber: string;
}

export interface PostPrincipal{
    groupName: string;
    groupAddress: string;
    groupEmail: string;
    groupCode: string;
    contactPerson: string;
    contactNumber: string;
}

export interface PrincipalELement{
    no: number;
    groupName: string;
    groupAddress: string;
    groupId: string;
    groupEmail: string;
    contactPerson: string;
    contactNumber: string;
}

// export interface PrincpalList{
//     groups: Principal;
// }


export interface Principals{
    groupName: string;
    groupAddress: string;
    groupId: string;
    groupEmail: string;
    contactPerson: string;
    contactNumber: string;
    env: string;
	createdAt: string;
	createdDate: string;
	createdTime: string;
	updatedAt: string;
	updatedDate: string;
	updatedTime: string;
	millis: number;
}