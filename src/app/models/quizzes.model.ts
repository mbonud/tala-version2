export interface QuizList {
	quizList: string[];
	next: string[];
	quizzes: Quizzes[];
}

export interface NextQuizzzesss {
	next: string;
	prev: string;
	quizzes: Quizzes;
}

export interface Quizzzesss {
	next: string;
	prev: string;
	quizzes: Quizzes;
}



export interface QuizList{
	id: number;
	question: string;
	answer: string;
	tags: string;
	choices: string;
}

export interface Quiz {
	quizUrl: string;
	rankId: [];
	quizThumbnail: string;
	quizType: string;
	quizName: string;
	quizId: string;
	quizFileSize: string;
	quizDescription: string;
	quizFileName: string;
	groupName: string;
	groupId: string;
	env: string;
	createdAt: string;
	createdDate: string;
	createdTime: string;
	updatedAt: string;
	updatedDate: string;
	updatedTime: string;
	millis: number;
	quizzes: Quiz[]
}

export interface QuizBody {	
	rankId: string[];
	quizThumbnail: string;
	quizType: string;
	quizName: string;	
	quizFileSize: string;
	quizDescription: string;
	quizFileName: string;
	// groupName: string;
	groupId: string;
	contentType: string;
	needPayment: string;
}

export interface Quizzes {
	quizUrl: string;
	rankId: [];
	quizThumbnail: string;
	quizType: string;
	quizName: string;
	quizId: string;
	quizFileSize: string;
	quizDescription: string;
	quizFileName: string;
	groupName: string;
	groupId: string;
	env: string;
	createdAt: string;
	createdDate: string;
	createdTime: string;
	updatedAt: string;
	updatedDate: string;
	updatedTime: string;
	millis: number;
	// quizzes: Quiz[]
}
