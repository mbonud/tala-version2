export interface Exam{
    data: ExamDetails[];
}

export interface ExamDetails{
    id: number;
    mainQuestion: string;
    subQuestion: string;
    choices: string;
    tags: string;
    rate: number;
    flipState: string;
}

export interface ExamResult {
  groupName: string;
    psychoId: string;
    userId: string;
    groupId: string;
    name: string;
    rank: string;
    mobileNo: string;
    email: string;
    age: number;
    assessmentDate: string;
    strategist: string;
    visionary: string;
    initiator: string;
    connector: string;
    builder: string;
    architect: string;
    totalItems: number;
    statusId: string;
}

export interface Stats{
    exams: StatusId[];
}

export interface StatusId {
  name: string;
  groupName: string;
    groupId: string;
    statusId: string;
    psychId: string;
    userId: string;
    payment: number;
    mobileNo: string;
    email: string;
    paymentDate: string;
    assignedDate: string;
    assessmentDate: string;
    result: string;
}