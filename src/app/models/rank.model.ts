export interface RankElements{
    no: number;
    body: Rank;
}

export interface Rank{ 
    ranks: [];
    rank: string;
    rankId: string;
}

export interface Ranks {
    rank: string;
    rankId: string;
	env: string;
	createdAt: string;
	createdDate: string;
	createdTime: string;
	updatedAt: string;
	updatedDate: string;
	updatedTime: string;
	millis: number;
    
}

export interface Ranksss {
    rank: string;
    value: string;
    disabled: boolean;
   }