export interface Users{
	next: string;
	prev: string;
	userList: User[];
}

export interface User {  
  	statusId: string;
	userId: string;
	email: string;
	password: string;	
	firstName: string;
	middleName: string;
	lastName: string;
	birthday: string;
	userType: string;
	crewNo: string;
	isVerified: string;
	code: string;
	mobile: string;
	rank: string;
	rankId: string;
	groupCode: string;
	groupId: string;
	groupName: string;
	gender: string;
	alias: string;
	icon: string;
	module: string;
	lastSync: string;
	createdAt: string;
	createdDate: string;
	createdTime: string;
	updatedAt: string;
	updatedDate: string;
	updatedTime: string;
	millis: number;
	name: string;
	no: number;
}


export interface NewUser {
  email: string;
  password: string;	
  firstName: string;
  middleName: string;
  lastName: string;
  birthday: string;
  userType: string;
  crewNo: string;    
  mobile: string;    
  rankId: string;
  groupId: string;  
  gender: string;  
}

export interface UserDash{
	cardlist: [];
	fullName: '';
}

export interface UserElements{
	no: number;
	userId: string;
	email: string;
	name: string;
	birthday: string;
	mobile: string;
	rank: string;
	gender: string;
	groupId: string;
	groupName: string;
	lastSync: string;
}

export interface PsychUsers{
	userId: [];
}

export interface DeleteUserList{
	userList: DeleteAssign[];
  }
  
  export interface DeleteAssign {
	userId: string;
	groupId: string;
  }