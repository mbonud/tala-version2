export interface Flag {
	channelId: string;
	userId: string;
	reportMessage: string;
	flagType: string;
	url: string;
	topic?: object;
	message?: object;
	reply?: object;
}
