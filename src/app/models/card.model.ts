export interface CardList {
	cardList: string[];
	nextSet: string[];
	cards: Card[];
}

export interface Card {
	cardId: string;
	cardName: string;
	cardDescription: string;
	cardUrl: string;
	cardType: string;
	groupId: string;
	cardRank: string;
	cardThumbnail: string;
	env: string;
	cardFileName: string;
	createdAt: string;
	createdDate: string;
	createdTime: string;
	updatedAt: string;
	updatedDate: string;
	updatedTime: string;
	millis: number;
}

