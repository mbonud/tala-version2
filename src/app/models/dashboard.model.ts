

export interface DifficultyBody {
    count: string;
    data: [];
}

export interface FlashcardElement {
    no: number;
    fullName: string;
    notseen: NotSeen;
    easy: Easy;
    medium: Medium;
    hard: Hard;
}

export interface FlashcardReports{
    no: number;
    fullName: string;
    cardId: string;
    cardOwnerId: string;
    notseen: NotSeen;
    easy: Easy;
    medium: Medium;
    hard: Hard;
}


export interface NotSeen{
    count: number;
    data: Data[];
}

export interface Easy{
    count: number;
    data: Data[];
}

export interface Medium{
    count: number;
    data: Data[];
}

export interface Hard{
    count: number;
    data: Data[];
}

export interface Data{
    tags: string;
    question: string;
    answer: string;
}

export interface Difficulty {
    easy: [];
    medium: [];
    hard: [];
    notseen: [];
}

export interface Data {
    answer: string;
    question: string;
    tags: string;
}

export interface CardSelection{
    cardName: string;
    cardId: string;
    group: string;
}

export interface CourseSelection{
    courseName: string;
    courseId: string;
}

export interface QuizListElement {
    quizId: string;
    quizName: string;
    crewFail: number;
    crewPass: number;
    ave: number;
    group: string;
}

export interface QuizReports{
    no: number;
    fullName: string;
    quizId: string;
    quizOwnerId: string;
    average: string;
    correct: Correct;
    wrong: Wrong;
}

export interface QuizListReports{
    quizId: string;
    group:string;
    quizName: string;
    crewFail: number;
    crewPass: number;
    ave: number;
    no: number;
}


export interface QuizElement {
    fullName: string;
    correct: Correct;
    wrong: Wrong;
    average: String;
}


export interface QuizSelection{
   quizName: string;
   quizId: string;
}

export interface QuizData{
    tags: string;
    count: string;
}

export interface QuizInnerData {
    count: number;
    tags: string;
    items: [];
}

export interface QuizInnerDataTable{
    no: number;
    question: string;
    answer: string;
}

export interface Correct {
    count: Number;
    data: [];
}

export interface Wrong {
    count: Number;
    data: [];
}

export interface CourseListElement {
    courseId: string;
    courseName: string;
    notStarted: number;
    notYetCompleted: number;
    completed: number;
    feedbackAve: number;
}

export interface CourseListReports {
    courseId: string;
    courseName: string;
    notStarted: number;
    notYetCompleted: number;
    completed: number;
    feedbackAve: number;
    group:string;
    no: number;
}

export interface CourseElement {
    courseId: string;
    courseName: string;
    courseOwnerId: string;
    fullName: string;
    status: string;
    feedback: number;
}

export interface CourseReport {
    courseId: string;
    courseName: string;
    courseOwnerId: string;
    fullName: string;
    status: string;
    feedback: number;
    comment: string;
    no: number;
}

export interface SeafarerElements {
    no: number;
	userId: string;
	email: string;
	name: string;
	birthday: string;
	mobile: string;
	rank: string;
	gender: string;
	groupId: string;
	lastSync: string;
}

export interface SeafarerReports{
    crewName: string;
    status: string;
    lastSync: string;
    no: number;
}

export interface ForGBTopThree{
    quizId: string;
    quizName: string;
    userId: string;
    name: string;
    totalScore: string;
    dateSubmitted: string;
    data: [];
}

export interface ForGBTopThreeDataTable{
    quizId: string;
    quizName: string;
    userId: string;
    name: string;
    totalScore: string;
    dateSubmitted: string;
    data: [];
 
}

export interface SeamulaDashboard{
    psychoId: string;
    userId: string;
    name: string;
    rank: string;
    groupId: string;
    groupName: string;
    mobileNo: string;
    age: number;
    email: string;
    assessmentDate: string;
    strategist: string;
    visionary: string;
    initiator: string;
    connector: string;
    builder: string;
    architect: string;
    totalItems: number;
    statusId: string;
}