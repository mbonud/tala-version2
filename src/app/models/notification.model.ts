

export interface Notification {
	notifId: string;
	markerId: string;
	type: string;
	url: string;
	username: string;
	seen: string;
	message: string;
	createdAt: string;
	title: object;
	channelId?: string;
	topicId?: string;
}

export interface NotificationList {
	notificationList: Notification[];
}
