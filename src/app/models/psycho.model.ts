export interface Psychological{
    psychoId : string;
    psychoDescription : string;
    psychoName: string;
	psychoCode : string;
    psychoFileName : string;
    thumbnail: string;
}



export interface PsychoTestList{
    exams: [];
}

export interface PTListDetails{
   psychoId: string;
   psychoName: string;
   psychoDescription: string;
   psychoCode: string;
   psychoFileName: string;
   thumbnail: string;
   env: string;
   createdAt: string;
   createdDate: string;
   createdTime: string;
   updatedAt: string;
   updatedDate: string;
   updatedTime: string;
   millis: number;
}