export interface Groups{
    createdAt: string;
    createdDate: string;
    createdTime: string;
    updatedAt: string;
    updatedDate: string;
    updatedTime: string;
    millis: string;
    groupName: string;
    groupAddress: string;
    groupId: string;
    groupCode: string;
    groupEmail: string;
    contactNumber: string;
    contactPerson: string;
    env: string;
    status: string;
}