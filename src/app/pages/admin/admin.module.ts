import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminRoutingModule } from './admin-routing.module';
import { LibraryComponent } from './library/library.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { NgxSelectModule } from 'ngx-select-ex';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [LibraryComponent, DashboardComponent],
  imports: [
    CommonModule,
    AdminRoutingModule,    
    NgxSelectModule,
    FormsModule,
    ReactiveFormsModule
  ],  
})
export class AdminModule { }
