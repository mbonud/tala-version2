import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {
  customNav = 'rgb(0,35,82)'
  navLink = 'white'
  constructor() { }

  ngOnInit(): void {}

  public onLogout(): void {
    console.log('press logout')
  }

  headColor(color: any){    
    this.customNav = color
  }

  headText(color: any){
    this.navLink = color
  }

}
