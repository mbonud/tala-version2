import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from './admin.component';
import { AuthGuard } from '@app/core/auth/auth.guard';
import { AuthGuardGuard } from '@app/auth-guard.guard';

const routes: Routes = [
  { path: '', component: AdminComponent },
  { 
    path: 'library', 
    canActivate: [AuthGuardGuard],
    loadChildren: () => import('./library/library.module').then(m => m.LibraryModule)
  },
  { 
    canActivate: [AuthGuardGuard],
    path: 'setup', 
    loadChildren: () => import('./setup/setup.module').then(m => m.SetupModule)
  },
  { 
    canActivate: [AuthGuardGuard],
    path: 'dashboard', 
    loadChildren: () => import('./dashboard/dashboard.module').then(m => m.DashboardModule)
  },
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class AdminRoutingModule { }
