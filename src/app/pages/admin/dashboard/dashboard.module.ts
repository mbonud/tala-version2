import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { SeafarerDashboardComponent } from './seafarer-dashboard/seafarer-dashboard.component';
import { QuizDashboardComponent } from './quiz-dashboard/quiz-dashboard.component';
import { CourseDashboardComponent } from './course-dashboard/course-dashboard.component';
import { FlashcardDashboardComponent } from './flashcard-dashboard/flashcard-dashboard.component';
import { NgxSelectModule } from 'ngx-select-ex';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DataTablesModule } from 'angular-datatables';
import { RatingModule } from 'ng-starrating';
@NgModule({
  declarations: [SeafarerDashboardComponent, QuizDashboardComponent, CourseDashboardComponent, FlashcardDashboardComponent],
  imports: [
    RatingModule,
    CommonModule,
    DashboardRoutingModule,
    NgxSelectModule,
    FormsModule,
    ReactiveFormsModule,
    DataTablesModule
  ]
})
export class DashboardModule { }
