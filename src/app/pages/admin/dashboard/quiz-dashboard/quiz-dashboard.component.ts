import { Component, OnInit, AfterViewInit, OnDestroy, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { PrincipalService } from '@app/shared/services/principal.service';
import { FormControl } from '@angular/forms';
import { UserService } from '@app/shared/services/user.service';
import { LibraryService } from '@app/shared/services/library.service';
import { DataTableDirective } from 'angular-datatables';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { ToastContainerDirective, ToastrService } from 'ngx-toastr';
import { DashboardQuizModalComponent } from '@app/shared/modal/dashboard/dashboard-quiz-modal/dashboard-quiz-modal.component';

@Component({
  selector: 'app-quiz-dashboard',
  templateUrl: './quiz-dashboard.component.html',
  styleUrls: ['./quiz-dashboard.component.css']
})
export class QuizDashboardComponent implements AfterViewInit, OnDestroy, OnInit {  
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;

  public selectPrincipal = new FormControl();
  isLoad = false;
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();  
  noDataFound = 'No data found!'
  principalList = []
  groupId;
  quizList = [];
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true
  };
  constructor(
    private principalService: PrincipalService,
    private UserService: UserService,
    private libraryService: LibraryService,
    private modalService: BsModalService,
    private toastr: ToastrService
  ) {
    this.selectPrincipal.valueChanges.subscribe(value => this.groupId = value)
   }

  ngOnInit(): void {    
    // fetch princial list
    this.principalService.fetchPrincialList('groups')
    .subscribe((res) => {
        this.principalList = JSON.parse(JSON.stringify(res.body.groups))  
        this.principalList.push({groupName: 'No Group', groupId: 'none'})      
    })    
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,      
    };
    this.extractData
    this.dtTrigger.next();
  }

  loadDatatables(){
    this.isLoad = true
    console.log('users?groupid=' + this.groupId)

    if(this.groupId){
      // auth/dashboard/groups/`+ group +`/courses/reports`;
     this.libraryService.fetchDashboardReports(`http://tala-maritime.ap-northeast-1.elasticbeanstalk.com/api/v1/auth/dashboard/groups/c785ba95f9161c0b069c696f448b24ba74516115/quizzes/reports`)
     // this.libraryService.fetchDashboardReports(`auth/dashboard/groups/`+ this.groupId +`/courses/reports`)
     .subscribe((res) => {
       console.log(res)
      this.isLoad = false
      this.rerender()        
      this.quizList = res.body.quizList
      this.extractData
      this.dtTrigger.next();
     },
     (error) => {
       this.rerender()
       this.isLoad = !this.isLoad
       this.noDataFound = 'No data found!'
       this.quizList = []
       this.dtTrigger.next();
     }
   )
   }else{
     this.rerender()
     this.isLoad = !this.isLoad
     this.noDataFound = 'No data found!'
     this.quizList = []
     this.dtTrigger.next();
   }
   
  }

  
  viewQuiz(coursesId: string, coursesName: string, groupId: string){
    const initialState = {
      data : { coursesId: coursesId, coursesName: coursesName, groupId: 'c785ba95f9161c0b069c696f448b24ba74516115' },      
    }

    this.modalRef = this.modalService.show(
      DashboardQuizModalComponent,
      Object.assign({initialState}, { class: 'modal-dialog-xl' }, this.config)
    );

    (<DashboardQuizModalComponent>this.modalRef.content).onClose.subscribe(result => {
      if (result === true) {
        this.toastr.success('Success!');        
      }
    });
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      // this.dtTrigger.next();
    });
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void { 
    this.dtTrigger.unsubscribe();    
  }

  private extractData(res: Response) {
    const body = res.body;
    console.log('extra body data '  + body)
    console.log('extra body data sadfasdf ')
    return body || {};
  }

}