import { Component, OnInit, AfterViewInit, OnDestroy, ViewChild } from '@angular/core';
import { Subject } from 'rxjs';
import { PrincipalService } from '@app/shared/services/principal.service';
import { FormControl } from '@angular/forms';
import { UserService } from '@app/shared/services/user.service';
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-seafarer-dashboard',
  templateUrl: './seafarer-dashboard.component.html',
  styleUrls: ['./seafarer-dashboard.component.css']
})
export class SeafarerDashboardComponent implements  AfterViewInit, OnDestroy, OnInit {  
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  
  public selectPrincipal = new FormControl();
  isLoad = false;  
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();  
  noDataFound = 'No data found!'
  principalList = []
  groupId;
  seafarerList = [];

  constructor(
    private principalService: PrincipalService,
    private UserService: UserService
  ) {
    this.selectPrincipal.valueChanges.subscribe(value => this.groupId = value)
   }

  ngOnInit(): void {    
    // fetch princial list
    this.principalService.fetchPrincialList('groups')
    .subscribe((res) => {
        this.principalList = JSON.parse(JSON.stringify(res.body.groups))      
        this.principalList.push({groupName: 'No Group', groupId: 'none'})  
    })    
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,      
    };
    this.extractData
    this.dtTrigger.next();
  }

  loadSeafarerList(){
    this.isLoad = true
    console.log('users?groupid=' + this.groupId)
    if(this.groupId){
      this.UserService.fetchSeafarerList('users?groupid=' + this.groupId)
        .subscribe((res) => {
          this.isLoad = false
          console.log(res.body.userList)
        
          this.rerender()        
          this.seafarerList = res.body.userList
          this.extractData
          this.dtTrigger.next();
        },
        (error) => {
          this.rerender()
          this.isLoad = !this.isLoad
          this.noDataFound = 'No data found!'
          this.seafarerList = []
          this.dtTrigger.next();
        }
      )

    }else{
      this.rerender()
      this.isLoad = !this.isLoad
      this.noDataFound = 'No data found!'
      this.seafarerList = []
      this.dtTrigger.next();
    }
    
  }

  rerender(): void {
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      // this.dtTrigger.next();
    });
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void { 
    this.dtTrigger.unsubscribe();    
  }

  private extractData(res: Response) {
    const body = res.body;
    console.log('extra body data '  + body)
    console.log('extra body data sadfasdf ')
    return body || {};
  }

}
