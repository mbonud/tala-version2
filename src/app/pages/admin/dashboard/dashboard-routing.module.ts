import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from '../admin.component';
import { SeafarerDashboardComponent } from './seafarer-dashboard/seafarer-dashboard.component';
import { FlashcardDashboardComponent } from './flashcard-dashboard/flashcard-dashboard.component';
import { CourseDashboardComponent } from './course-dashboard/course-dashboard.component';
import { QuizDashboardComponent } from './quiz-dashboard/quiz-dashboard.component';


const routes: Routes = [
  { path: '', component: AdminComponent,
    children: [
      { path: 'seafarer', component: SeafarerDashboardComponent },
      { path: 'flashcard', component: FlashcardDashboardComponent },
      { path: 'course', component: CourseDashboardComponent },
      { path: 'quiz', component: QuizDashboardComponent }
    ],
  },  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
