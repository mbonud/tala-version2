import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { PrincipalService } from '@app/shared/services/principal.service';
import { FormControl } from '@angular/forms';
import { UserService } from '@app/shared/services/user.service';
import { LibraryService } from '@app/shared/services/library.service'

@Component({
  selector: 'app-flashcard-dashboard',
  templateUrl: './flashcard-dashboard.component.html',
  styleUrls: ['./flashcard-dashboard.component.css']
})
export class FlashcardDashboardComponent implements OnInit {

  public selectPrincipal = new FormControl();
  isLoad = false;  
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();  
  noDataFound = 'No data found!'
  principalList = []
  groupId;
  flashcardList = [];

  constructor(
    private principalService: PrincipalService,
    private UserService: UserService,
    private libraryService: LibraryService
  ) {
    this.selectPrincipal.valueChanges.subscribe(value => this.groupId = value)
   }

  ngOnInit(): void {    
    // fetch princial list
    this.principalService.fetchPrincialList('groups')
    .subscribe((res) => {
        this.principalList = JSON.parse(JSON.stringify(res.body.groups))   
        this.principalList.push({groupName: 'No Group', groupId: 'none'})     
    })    
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,      
    };
    this.extractData
    this.dtTrigger.next();
  }

  loadDatatables(){
    console.log('users?groupid=' + this.groupId)
    // this.UserService.fetchUserListFlashCard('users?groupid=' + this.groupId)
    let urlFC = "http://tala-maritime.ap-northeast-1.elasticbeanstalk.com/api/v1/flashcards?cardType=fd3dbc02586f3eafb11350792c45ae2b73dbf95f" 
    this.libraryService.fetchDashboardReports(urlFC)
      .subscribe((res) => {
        console.log(res.body.userList)
        this.flashcardList = res.body.userList
      })
  }

  private extractData(res: Response) {
    const body = res.body;
    return body || {};
  }

}
