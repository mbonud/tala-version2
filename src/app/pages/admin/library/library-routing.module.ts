import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { LibraryComponent } from './library.component';
import { FlashCardComponent } from './flash-card/flash-card.component';
import { AdminComponent } from '../admin.component';
import { CourseComponent } from './course/course.component';
import { QuizComponent } from './quiz/quiz.component';

const routes: Routes = [
  { path: '', component: AdminComponent,
    children: [
      { path: 'flashcard', component: FlashCardComponent },
      { path: 'course', component: CourseComponent },
      { path: 'quiz', component: QuizComponent }
    ],
  },  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class LibraryRoutingModule { }
