import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { LibraryRoutingModule } from './library-routing.module';
import { FlashCardComponent } from './flash-card/flash-card.component';
import { CourseComponent } from './course/course.component';
import { DataTablesModule } from 'angular-datatables';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { NgxSelectModule } from 'ngx-select-ex';
import { ModalModule } from 'ngx-bootstrap/modal';
import { QuizComponent } from './quiz/quiz.component';

@NgModule({
  declarations: [
    FlashCardComponent,
    CourseComponent,
    QuizComponent 
  ],
  imports: [
    CommonModule,
    LibraryRoutingModule,
    DataTablesModule,
    FormsModule,
    ReactiveFormsModule,
    NgxSelectModule,
    CommonModule,
    ModalModule.forRoot()
  ]
})
export class LibraryModule { }
