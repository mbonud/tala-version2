import { Injectable } from '@angular/core';
import { Observable, throwError, from } from 'rxjs';
import { HttpResponse, HttpErrorResponse, HttpClient } from '@angular/common/http';
import { catchError, mergeMap } from 'rxjs/operators'
import { QuizList } from '@app/models/quizzes.model';

@Injectable({
  providedIn: 'root'
})
export class QuizService {

  constructor(
    private httpCient: HttpClient
  ) { }

  fetchQuiz(url: string): Observable<HttpResponse<QuizList>> {
		return this.httpCient		
			.get<QuizList>(url, { observe: 'response' })
			.pipe(
				catchError((error: HttpErrorResponse) => throwError(error))
			);
  }
  
}
