import { Component, OnInit, TemplateRef, ViewChild, AfterViewInit, OnDestroy } from '@angular/core';
import {FormControl} from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs/internal/Subject';
import { PrincipalService } from '@app/shared/services/principal.service';
import { ToastrService } from 'ngx-toastr';

import { QuizService } from './quiz.service'
import { Quiz,  Quizzes } from '@app/models/quizzes.model';
import { AddModalComponent } from '@app/shared/modal/library/add-modal/add-modal.component';
import { EditModalComponent } from '@app/shared/modal/library/edit-modal/edit-modal.component';
import { DeleteModalComponent } from '@app/shared/modal/library/delete-modal/delete-modal.component'
import { INgxSelectOption } from 'ngx-select-ex';
import { AssignModalComponent } from '@app/shared/modal/library/assign-modal/assign-modal.component';
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-quiz',
  templateUrl: './quiz.component.html',
  styleUrls: ['./quiz.component.css']
})
export class QuizComponent implements AfterViewInit, OnDestroy, OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  
  public selectedGroup: INgxSelectOption[];
  public selectPrincipal = new FormControl();
  public selectStatus = new FormControl();
  modalRef: BsModalRef;    
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  groupId = ''
  groupName;
  status = ''
  itemType = 'quizzes'
  config = {
    backdrop: true,
    ignoreBackdropClick: true
  };
  stats = [
    { status: 'active', activeStatus: true, title: 'Disable this', i: 0},
    { status: 'inactive', activeStatus: false, title: 'Enable this', i: 1 }
  ]
  principalList = [];
  isLoad = false
  activeStatus;
  // quizzes: Quiz[];
  quizzes;
  noDataFound = 'No data found!'

  constructor(
    private principalService: PrincipalService,    
    private modalService: BsModalService,
    private toastr: ToastrService,
    private quizService: QuizService
  ) { 
    this.selectPrincipal.valueChanges.subscribe(value => this.groupId = value)
    this.selectStatus.valueChanges.subscribe(value => this.status = value)
  }  

  ngOnInit(): void {
    this.isLoad = !this.isLoad
    // fetch princial list
    this.principalService.fetchPrincialList('groups')
    .subscribe((res) => {
        this.principalList = JSON.parse(JSON.stringify(res.body.groups))
        this.principalList.push({groupName: 'No Group', groupId: 'none'})
        this.isLoad = !this.isLoad
    })

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,      
    };  
  }  

  searchData(){
    this.isLoad = !this.isLoad
    let groupId = this.groupId
    if(this.groupId && this.status ){
      this.status = this.status.toLowerCase().trim()
      this.activeStatus = this.stats.find(({ status }) => status === this.status).activeStatus    
      // // get list of course
      let url = 'quizzes?quizType=group&groupid=' + groupId + '&status=' + this.status
      console.log(url)
      this.quizService.fetchQuiz(url)
      .subscribe((res) => {
        this.isLoad = !this.isLoad
        // this.dtOptions = {
        //   pagingType: 'full_numbers',
        //   pageLength: 5,         
        //   destroy: true,
        //   retrieve:true,
        //   paging: false        
        // };
        this.rerender()
        
        // const quiz: Quiz = res.body;
        // this.quizzes = quiz.quizzes
        this.quizzes = res.body.quizzes
        this.extractData   
        this.dtTrigger.next();
      },
      (error) => {
        this.rerender()
        this.isLoad = !this.isLoad
        this.noDataFound = 'No data found!'
        this.quizzes = []
        this.dtTrigger.next();
      }
      )
    }  else {
      this.rerender()
      this.isLoad = !this.isLoad
      this.noDataFound = 'No data found!'
      this.quizzes = []
      this.dtTrigger.next();     
    }
    
  }

  doSelectionChanges(options: INgxSelectOption[]) {
    this.selectedGroup = options;
    this.groupName = options[0].text
    console.log('doSelectionChanges', options);
  }

  openAddDialog(){
    const initialState = {
      data : { itemType: this.itemType },
      title: 'Add Quiz details.'
    }

    this.modalRef = this.modalService.show(
      AddModalComponent,
      Object.assign({initialState}, { class: 'modal-dialog-xl' }, this.config)
    );

    (<AddModalComponent>this.modalRef.content).onClose.subscribe(result => {
      if (result === true) {        
        this.toastr.success('Success!');
        this.searchData()
      }
    });
  }

  openEditDialog(id: string){
    const initialState = {
      data : { itemId: id, itemType: this.itemType, status: this.status,groupId: this.groupId, },
      title: 'Edit Quiz details.'
    }
    
    this.modalRef = this.modalService.show(
      EditModalComponent,
      Object.assign({initialState}, { class: 'modal-dialog-xl' }, this.config)
    );  

    (<EditModalComponent>this.modalRef.content).onClose.subscribe(result => {
      if (result === true) {        
        this.toastr.success('Success!');
        this.searchData()
      }
    });
  }

  openAssignDialog(id: string, needPayment: string, assignItem: string){
    const initialState = {
      data : { 
        itemId: id, itemType: this.itemType, needPayment: needPayment, assignItem: assignItem,
        group: {groupId: this.groupId, groupName: this.groupName},
      },
      title: 'Assign User'
    }     
    this.modalRef = this.modalService.show(
      AssignModalComponent,
      Object.assign({initialState}, { class: 'modal-dialog-xl' }, this.config)
    );    

    (<AssignModalComponent>this.modalRef.content).onClose.subscribe(result => {
      if (result === true) {        
        this.toastr.success('Success!');
        this.searchData()
      }else {
        this.dtTrigger.unsubscribe();
        // this.searchData()
      }
    });
  }
  
  openDeleteDialog(id: string, contentName: string) {
    const initialState = {
      data : { itemId: id, itemType: this.itemType, status: this.status },      
      title: this.stats.find(({ status }) => status === this.status).title + ' ' + contentName + '?',
    }
    this.modalRef = this.modalService.show(DeleteModalComponent,
      Object.assign({initialState}, this.config)
    );

    (<DeleteModalComponent>this.modalRef.content).onClose.subscribe(result => {
      if (result === true) {        
        this.toastr.success('Success!');
        this.searchData()
      }
    });
  }

  rerender(): void {  
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      // this.dtTrigger.next();
    });
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {    
    this.dtTrigger.unsubscribe();    
  }

  private extractData(res: Response) {
    const body = res.body;
    return body || {};
  }

}
