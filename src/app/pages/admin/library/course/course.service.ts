import { Injectable } from '@angular/core';
import { Observable, throwError, from } from 'rxjs';
import { HttpResponse, HttpErrorResponse, HttpClient } from '@angular/common/http';
import { catchError, mergeMap, map } from 'rxjs/operators';
import { Course, CourseList } from '@app/models/course.model';

@Injectable({
  providedIn: 'root'
})
export class CourseService {

  constructor(
    private httpClient: HttpClient
  ) { }

    fetchCourse(url: string): Observable<HttpResponse<CourseList>>{
      return this.httpClient
      .get<CourseList>(url, { observe: 'response' })
      .pipe(
        map(res => res),
        catchError((error: HttpErrorResponse) => throwError(error))
      )
    }
    

}
