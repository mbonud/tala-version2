import { Component, OnInit, resolveForwardRef, OnDestroy, ViewEncapsulation, TemplateRef, Input, ViewChild, AfterViewInit  } from '@angular/core';
import { CourseService } from './course.service'
import { Course } from '@app/models/course.model'
import { PrincipalService } from '@app/shared/services/principal.service'
import {FormControl} from '@angular/forms';
import { Subject } from 'rxjs';
import { NgForm } from '@angular/forms';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { ToastContainerDirective, ToastrService } from 'ngx-toastr';
import { AddModalComponent } from '@app/shared/modal/library/add-modal/add-modal.component';
import { EditModalComponent } from '@app/shared/modal/library/edit-modal/edit-modal.component';
import { DeleteModalComponent } from '@app/shared/modal/library/delete-modal/delete-modal.component'
import { AssignModalComponent } from '@app/shared/modal/library/assign-modal/assign-modal.component';
import { INgxSelectOption } from 'ngx-select-ex';
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-course',
  templateUrl: './course.component.html',
  styleUrls: ['./course.component.css']
})
export class CourseComponent implements AfterViewInit, OnDestroy, OnInit {  
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  
  public selectedGroup: INgxSelectOption[];
  public selectPrincipal = new FormControl();
  public selectStatus = new FormControl();
  modalRef: BsModalRef;
  config = {
    backdrop: true,
    ignoreBackdropClick: true
  };
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  groupId = ''
  groupName = ''
  status = ''
  itemType = 'courses'
  stats = [
    { status: 'active', activeStatus: true, title: 'Disable this ', i: 0},
    { status: 'inactive', activeStatus: false, title: 'Enable this ', i: 1 },
    { status: 'special', activeStatus: 'special', title: 'Enable this ', i: 1 }
  ]
  principalList = [];
  isLoad = false
  activeStatus;
  // courses: Course[] = [];
  courses;
  noDataFound = 'No data found!'

  constructor(
    private courseService: CourseService,
    private principalService: PrincipalService,
    private modalService: BsModalService,
    private toastr: ToastrService
  ) {  
    this.selectPrincipal.valueChanges.subscribe(value => this.groupId = value)
    this.selectStatus.valueChanges.subscribe(value => this.status = value)
   }

  ngOnInit(): void {
    let tName = 'https://bts-tala-lms.s3-ap-northeast-1.amazonaws.com/dev/api/v1/files/courses/kipkip.jpg'
    console.log(tName.split('/').pop())
    console.log(tName.replace(/^.*[\\\/]/, ''))

    this.isLoad = true
    // fetch princial list
    this.fetchPricipal();
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,      
    };

    // this.courseService.fetchCourse('courses?courseType=group&groupid=22b6eef6a174361bc1ec053d2d0d8825534ff025&status=active')    
    // .subscribe((res) => {
    //   this.isLoad = false
    //   this.dtOptions = {
    //     pagingType: 'full_numbers',
    //     pageLength: 5,         
    //     destroy: true,
    //     retrieve:true,
    //     paging: false        
    //   };
    //   const courses: Course = res.body;
    //   this.courses = courses.courses
    //   console.log(courses.courses)
    //   // this.extractData 
    //   this.dtTrigger.next();
    // },
    // (error) => {
    //   console.log('eeer')
    //   this.isLoad = false
    //   this.courses = []
    // }
    // )
  

  }
  fetchPricipal(){
    this.principalService.fetchPrincialList('groups')
      .subscribe((res) => {
      console.log('res body groups ' + JSON.parse(JSON.stringify(res.body)))
      this.principalList = JSON.parse(JSON.stringify(res.body.groups))
      this.principalList.push({groupName: 'No Group', groupId: 'none'})
      console.log(this.principalList)
      this.isLoad = false
    })
  }
  

  doSelectionChanges(options: INgxSelectOption[]) {
    this.selectedGroup = options;
    this.groupName = options[0].text
    console.log('doSelectionChanges', options);
  }
  
  searchData(){
    this.isLoad = true
    let groupId = this.groupId
    
    console.log(groupId + ' ' + this.status)
    // get list of course
    if(this.groupId && this.status ){
      this.status = this.status.toLowerCase().trim()
      this.activeStatus = this.stats.find(({ status }) => status === this.status).activeStatus
      let url = 'courses?courseType=group&groupid=' + groupId + '&status=' + this.status
      // let url = 'courses'
      this.courseService.fetchCourse(url)
      .subscribe((res) => {
        
        console.log(JSON.stringify(res.body))
        this.isLoad = false
        // this.dtOptions = {
        //   pagingType: 'full_numbers',
        //   pageLength: 5,         
        //   destroy: true,
        //   retrieve:true,
        //   paging: false        
        // };
        
        // const courses: Course = res.body;
        this.rerender()
        // this.courses = courses.courses
        this.courses = res.body.courses
        // console.log(courses.courses)
        this.extractData
        this.dtTrigger.next();
      },
      (error) => {
        this.rerender()        
        this.isLoad = false
        this.noDataFound = 'No data found!'
        this.courses = []
        this.dtTrigger.next();
      }
      )
    } else {
        this.rerender()        
        this.isLoad = false
        this.noDataFound = 'No data found!'
        this.courses = []
        this.dtTrigger.next();
    }
    
  }

  openAddDialog(){
    const initialState = {
      data : { itemType: this.itemType},
      title: 'Add Course details.'
    }

    this.modalRef = this.modalService.show(
      AddModalComponent,
      Object.assign({initialState}, { class: 'modal-dialog-xl' }, this.config)
    );

    (<AddModalComponent>this.modalRef.content).onClose.subscribe(result => {
      if (result === true) {
        this.toastr.success('Success!');
        this.searchData()
      }
    });
  }

  openEditDialog(id:string){
    const initialState = {
      data : { itemId: id, itemType: this.itemType, status: this.status,
      groupId: this.groupId, groupName: this.groupName.toUpperCase() },
      title: 'Edit Course details.'
    }     
    this.modalRef = this.modalService.show(
      EditModalComponent,
      Object.assign({initialState}, { class: 'modal-dialog-xl' }, this.config)
    );    

    (<EditModalComponent>this.modalRef.content).onClose.subscribe(result => {
      if (result === true) {        
        this.toastr.success('Success!');
        this.searchData()
      }
    });
  }
  

  openAssignDialog(id: string, needPayment: string, assignItem: string){
    const initialState = {
      data : { 
        itemId: id, itemType: this.itemType, needPayment: needPayment, assignItem: assignItem,
        group: {groupId: this.groupId, groupName: this.groupName},
      },
      title: 'Assign User'
    }     
    this.modalRef = this.modalService.show(
      AssignModalComponent,
      Object.assign({initialState}, { class: 'modal-dialog-xl' }, this.config)
    );    

    (<AssignModalComponent>this.modalRef.content).onClose.subscribe(result => {
      if (result === true) {        
        this.toastr.success('Success!');
        this.searchData()
      }else {
        this.dtTrigger.unsubscribe();
        // this.searchData()
      }
    });
  }

  openDeleteDialog(id: string, contentName: string) {
    const initialState = {
      data : { itemId: id, itemType: this.itemType, status: this.status },      
      title: this.stats.find(({ status }) => status === this.status).title + ' ' + contentName + '?',
    }
    this.modalRef = this.modalService.show(
      DeleteModalComponent,
      Object.assign({initialState}, this.config)
    );

    (<DeleteModalComponent>this.modalRef.content).onClose.subscribe(result => {
      if (result === true) {        
        this.toastr.success('Success!');
        this.searchData()
      }
    });
  }

 

  rerender(): void {  
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      // this.dtTrigger.next();
    });
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void { 
    this.dtTrigger.unsubscribe();    
  }

  private extractData(res: Response) {
    const body = res.body;
    console.log('extra body data '  + body)
    console.log('extra body data sadfasdf ')
    return body || {};
  }

}
