import { Injectable } from '@angular/core';
import { Observable, throwError, from } from 'rxjs';
import { HttpResponse, HttpErrorResponse, HttpClient } from '@angular/common/http';
import { catchError, mergeMap } from 'rxjs/operators';
import { Card } from '@app/models/card.model';
import { Cards, CardList } from '@app/models/flash-card.model';

@Injectable({
  providedIn: 'root'
})
export class FlashCardService {

  constructor(
    private httpCient: HttpClient
  ) { }

  fetchFlashCard(url: string): Observable<HttpResponse<CardList>>{
    return this.httpCient
    .get<CardList>(url, { observe: 'response' })
    .pipe(
      catchError((error: HttpErrorResponse) => throwError(error))
    )
  }
}
