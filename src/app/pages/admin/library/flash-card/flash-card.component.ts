import { Component, OnInit, TemplateRef, OnDestroy, ViewChild, AfterViewInit } from '@angular/core';
import {FormControl} from '@angular/forms';
import { BsModalService, BsModalRef } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs/internal/Subject';
import { PrincipalService } from '@app/shared/services/principal.service';
import { ToastrService } from 'ngx-toastr';

import { FlashCardService } from './flash-card.service'
import { FlashCard, FlashCards, CardList } from '@app/models/flash-card.model';
import { AddModalComponent } from '@app/shared/modal/library/add-modal/add-modal.component';
import { EditModalComponent } from '@app/shared/modal/library/edit-modal/edit-modal.component';
import { DeleteModalComponent } from '@app/shared/modal/library/delete-modal/delete-modal.component'
import { INgxSelectOption } from 'ngx-select-ex';
import { AssignModalComponent } from '@app/shared/modal/library/assign-modal/assign-modal.component';
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-flash-card',
  templateUrl: './flash-card.component.html',
  styleUrls: ['./flash-card.component.css']
})
export class FlashCardComponent implements AfterViewInit, OnDestroy, OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  
  public selectedGroup: INgxSelectOption[];
  public selectPrincipal = new FormControl();  
  public selectStatus = new FormControl();
  modalRef: BsModalRef;    
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();
  groupId = ''
  groupName = ''
  status = ''
  itemType = 'flashcards'
  config = {
    backdrop: true,
    ignoreBackdropClick: true
  };
  stats = [
    { status: 'active', activeStatus: true, title: 'Disable this ', i: 0},
    { status: 'inactive', activeStatus: false, title: 'Enable this ', i: 1 }
  ]
  principalList = [];
  isLoad = false
  activeStatus;
  // flashCards: FlashCard[];
  flashCards;
  cards = []
  noDataFound = 'No data found!'

  constructor(
    private principalService: PrincipalService,
    private flashCardService: FlashCardService,
    private modalService: BsModalService,
    private toastr: ToastrService
  ) {
    this.selectPrincipal.valueChanges.subscribe(value => this.groupId = value)
    this.selectStatus.valueChanges.subscribe(value => this.status = value)
  }
 
  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }

  ngOnInit(): void {
    this.isLoad = !this.isLoad
    // fetch princial list
    this.principalService.fetchPrincialList('groups')
    .subscribe((res) => {
        this.principalList = JSON.parse(JSON.stringify(res.body.groups))  
        this.principalList.push({groupName: 'No Group', groupId: 'none'})      
        this.isLoad = !this.isLoad
    })

    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,      
    };  
  }

  openEditDialog(id: string){
    const initialState = {
      data : { itemId: id, itemType: this.itemType, status: this.status, groupId: this.groupId, },
      title: 'Edit Flash Card details.'
    }
    
    this.modalRef = this.modalService.show(
      EditModalComponent,
      Object.assign({initialState}, { class: 'modal-dialog-xl' }, this.config)
    );  

    (<EditModalComponent>this.modalRef.content).onClose.subscribe(result => {
      console.log(result)
      if (result === true) {
        console.log('true')
        this.toastr.success('Success!');
        this.searchData()
      } else {
        console.log('else')
      }
    });
  }

  doSelectionChanges(options: INgxSelectOption[]) {
    this.selectedGroup = options;
    this.groupName = options[0].text
    console.log('doSelectionChanges', options);
  }

  openAssignDialog(id: string, needPayment: string, assignItem: string){
    const initialState = {
      data : { 
        itemId: id, itemType: this.itemType, needPayment: needPayment, assignItem: assignItem,
        group: {groupId: this.groupId, groupName: this.groupName},
      },
      title: 'Assign User'
    }     
    this.modalRef = this.modalService.show(
      AssignModalComponent,
      Object.assign({initialState}, { class: 'modal-dialog-xl' }, this.config)
    );    

    (<AssignModalComponent>this.modalRef.content).onClose.subscribe(result => {
      if (result === true) {        
        this.toastr.success('Success!');
        this.searchData()
      }else {
        this.dtTrigger.unsubscribe();
        // this.searchData()
      }
    });
  }

  openAddDialog(){
    const initialState = {
      data : { itemType: this.itemType },
      title: 'Add Flashcard details.'
    }

    this.modalRef = this.modalService.show(
      AddModalComponent,
      Object.assign({initialState}, { class: 'modal-dialog-xl' }, this.config)
    );

    (<AddModalComponent>this.modalRef.content).onClose.subscribe(result => {
      if (result === true) {        
        this.toastr.success('Success!');
        this.searchData()
      }
    });
  }

  openDeleteDialog(id: string, contentName: string) {
    const initialState = {
      data : { itemId: id, itemType: this.itemType, status: this.status },      
      title: this.stats.find(({ status }) => status === this.status).title + ' ' + contentName + '?',
    }
    this.modalRef = this.modalService.show(
      DeleteModalComponent,
      Object.assign({initialState}, this.config)
    );

    (<DeleteModalComponent>this.modalRef.content).onClose.subscribe(result => {
      if (result === true) {        
        this.toastr.success('Success!');
        this.searchData()
      }
    });
  }

  searchData(){
    this.isLoad = !this.isLoad
    let groupId = this.groupId
    if(this.groupId && this.status ){
      this.status = this.status.toLowerCase().trim()
      this.activeStatus = this.stats.find(({ status }) => status === this.status).activeStatus        
      // get list of course
      let url = 'flashcards?cardType=group&groupid=' + groupId + '&status=' + this.status        
      this.flashCardService.fetchFlashCard(url)
      .subscribe((res) => {
        this.isLoad = !this.isLoad        
        // this.dtOptions = {
        //   pagingType: 'full_numbers',
        //   pageLength: 5,         
        //   destroy: true,
        //   retrieve:true,
        //   paging: false        
        // };
        this.rerender()
        
        // const card: FlashCard = res.body;
        // this.flashCards = card.cards;  
        console.log(res.body)
        this.flashCards = res.body.cards
        this.extractData   
        this.dtTrigger.next();
      },
      (error) => {
        this.rerender()
        this.isLoad = !this.isLoad
        this.noDataFound = 'No data found!'
        this.flashCards = []
        this.dtTrigger.next();
      }
      )
    } else {
        this.rerender()
        this.isLoad = !this.isLoad
        this.noDataFound = 'No data found!'
        this.flashCards = []
        this.dtTrigger.next();
    }

    
  }

  rerender(): void {  
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      // this.dtTrigger.next();
    });
  }

  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {  
    this.dtTrigger.unsubscribe();    
  }

  private extractData(res: Response) {
    const body = res.body;
    return body || {};
  }

}
