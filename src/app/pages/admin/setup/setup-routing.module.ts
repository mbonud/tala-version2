import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AdminComponent } from '../admin.component';
import { GroupComponent } from './group/group.component';
import { PyschologicalExamComponent } from './pyschological-exam/pyschological-exam.component';
import { RankComponent } from './rank/rank.component';
import { UserComponent } from './user/user.component';

const routes: Routes = [
  { path: '', component: AdminComponent,
    children: [
      { path: 'group', component: GroupComponent },
      { path: 'psychologicalExam', component: PyschologicalExamComponent },
      { path: 'rank', component: RankComponent },
      { path: 'user', component: UserComponent }
    ],
  },  
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SetupRoutingModule { }
