import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';
import { FormControl } from '@angular/forms';
import { UserService } from '@app/shared/services/user.service'
import { PsychologyService } from '@app/shared/services/psychology.service'

@Component({
  selector: 'app-psychological-test-exam',
  templateUrl: './psychological-test-exam.component.html',
  styleUrls: ['./psychological-test-exam.component.css']
})
export class PsychologicalTestExamComponent implements OnInit {
  public selectExam = new FormControl();
  isLoad = false;
  modalRef: BsModalRef;    
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();  
  noDataFound = 'No data found!'
  testExamList = []
  psychologicalList = []
  examId;

  constructor(
    private userService: UserService,
    private psychologyService: PsychologyService
  ) { 
    this.selectExam.valueChanges.subscribe(value => this.examId = value)
  }

  ngOnInit(): void {
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,      
    };
    this.fetchExamList()
  }

  fetchExamList(){
    this.psychologyService.fetchPyschologyList('auth/psych/exams')
    .subscribe((res) => {
      console.log('exam list ' + res.body.exams)
      this.psychologicalList = res.body.exams
    })
  }

  loadTestExam(){
    let url = `auth/psych/${this.examId}/users`;
    this.userService.fetchUserListEligible(url)
    .subscribe((res) => {
      this.testExamList = res.body
      console.log(JSON.stringify(this.testExamList))
      this.isLoad = false
      this.extractData
      this.dtTrigger.next();
    },
    (error) => {
      this.isLoad = false
      this.noDataFound = 'No data found!'
    }
    )

  }

  private extractData(res: Response) {
    const body = res.body;
    return body || {};
  }

}
