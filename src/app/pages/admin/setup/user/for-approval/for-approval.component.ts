import { Component, OnInit } from '@angular/core';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';
import { FormControl } from '@angular/forms';
import { PrincipalService } from '@app/shared/services/principal.service';

@Component({
  selector: 'app-for-approval',
  templateUrl: './for-approval.component.html',
  styleUrls: ['./for-approval.component.css']
})
export class ForApprovalComponent implements OnInit {
  public selectPrincipal = new FormControl();
  isLoad = false;
  modalRef: BsModalRef;    
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();  
  noDataFound = 'No data found!'
  principalList = []
  groupId;
  approvalList = [];

  constructor(
    private principalService: PrincipalService,
  ) { 
    this.selectPrincipal.valueChanges.subscribe(value => this.groupId = value)
  }

  ngOnInit(): void {
    this.isLoad = !this.isLoad
    // fetch princial list
    this.principalService.fetchPrincialList('groups')
    .subscribe((res) => {
        this.principalList = JSON.parse(JSON.stringify(res.body.groups))        
        this.isLoad = !this.isLoad
    })
    this.fetchForApprovalList()
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,      
    };  
  }

  fetchForApprovalList(){

  }

  loadTestExam(){
    
  }

  private extractData(res: Response) {
    const body = res.body;
    return body || {};
  }

}
