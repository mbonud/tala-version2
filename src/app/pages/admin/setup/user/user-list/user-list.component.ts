import { Component, OnInit, OnDestroy, AfterViewInit, ViewChild } from '@angular/core';
import {FormControl} from '@angular/forms';
import { Subject } from 'rxjs';
import { NgForm } from '@angular/forms';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { UserService } from '@app/shared/services/user.service'
import { ToastrService } from 'ngx-toastr';
import { SessionService } from '@app/core/session/session.service';
import { AddModalUserComponent } from '@app/shared/modal/user/add-modal-user/add-modal-user.component';
import { PrincipalService } from '@app/shared/services/principal.service';
import { EditModalUserComponent } from '@app/shared/modal/user/edit-modal-user/edit-modal-user.component';
import { DeleteModalUserComponent } from '@app/shared/modal/user/delete-modal-user/delete-modal-user.component';
import { map } from 'rxjs/operators';
import { DataTableDirective } from 'angular-datatables/src/angular-datatables.directive';
import { INgxSelectOption } from 'ngx-select-ex';

@Component({
  selector: 'app-user-list',
  templateUrl: './user-list.component.html',
  styleUrls: ['./user-list.component.css']
})
export class UserListComponent implements AfterViewInit, OnDestroy, OnInit {
  @ViewChild(DataTableDirective)
  dtElement: DataTableDirective;
  
  public selectedGroup: INgxSelectOption[];
  public selectPrincipal = new FormControl();
  isLoad = false;
  modalRef: BsModalRef;    
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();  
  noDataFound = 'No data found!'
  userdata = this.sessionService.getValue('user');
  userUrl;
  userList;
  name;
  groupId;
  principalList = [];
  config = {
    backdrop: true,
    ignoreBackdropClick: true
  };
  

  constructor(
    private userService: UserService,
    private sessionService: SessionService,
    private modalService: BsModalService,
    private principalService: PrincipalService,
    private toastr: ToastrService
  ) { 
    this.selectPrincipal.valueChanges.subscribe(value => this.groupId = value)
  }

  ngOnInit(): void {
    console.log('extar')
    this.fetchPricipal();
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,
    };
    // this.userUrl = 'admin/groups/1aa9673dfd40e253942d6f753397813e49ef3658/users'
      // this.loadUserList(this.userUrl)
    // if(this.userdata){
    //   // this.userUrl = 'auth/users?groupid=' + this.userdata.groupId + '&limit=';
    //   // this.userUrl = 'users?groupid=' + this.userdata.groupId + '&limit=';
    //   this.userUrl = 'api/v1/admin/groups/1aa9673dfd40e253942d6f753397813e49ef3658/users'
    //   this.loadUserList(this.userUrl)
    // }
  }

  fetchPricipal(){
    this.principalService.fetchPrincialList('groups')
      .subscribe((res) => {
      console.log('res body groups ' + JSON.parse(JSON.stringify(res.body)))
      this.principalList = JSON.parse(JSON.stringify(res.body.groups))
      this.principalList.push({groupName: 'No Group', groupId: 'none'})
      this.isLoad = false
    })
  }

  searchUserList(){
    // console.log(url)
    this.loadUserList()
  }

  openAddUserModal(){
    const initialState = {      
      title: 'Add User details.'
    }

    this.modalRef = this.modalService.show(
      AddModalUserComponent,
      Object.assign({initialState}, { class: ' modal-lg' }, this.config)
    );

    (<AddModalUserComponent>this.modalRef.content).onClose.subscribe(result => {
      if (result === true) {        
        this.toastr.success('Success!');
        this.loadUserList()
      }
    });
  }

  openEditUserDialog(userId: string){
    // let userData = this.userList.find(user => user.userId === userId)
    // console.log(userData)    
    const initialState = {
      userId: userId,
      title: 'Edit User details.'
    }

    this.modalRef = this.modalService.show(
      EditModalUserComponent,
      Object.assign({initialState}, { class: ' modal-lg' }, this.config)
    );

    (<EditModalUserComponent>this.modalRef.content).onClose.subscribe(result => {
      console.log(result)
      if (result === true) {
        console.log('if true')
        // console.log('true')
        this.toastr.success('Success!');
        this.loadUserList()
      } else {
        console.log('esle false')
      }
    });
  }

  openDeleteUserDialog(userId, name: string){
    const initialState = {
      userId: userId,
      title: 'Do you want to delete this '+ name +'?'
    }

    this.modalRef = this.modalService.show(
      DeleteModalUserComponent,
      Object.assign({initialState}, this.config)
    );

    (<DeleteModalUserComponent>this.modalRef.content).onClose.subscribe(result => {
      if (result === true) {        
        this.toastr.success('Success!');
        this.loadUserList()
      }
    });
  }

  loadUserList(): void {
    
    // this.userList = []
    // this.dtTrigger.unsubscribe();
    // this.dtTrigger.subscribe();
    if(this.groupId){
      let url = 'admin/groups/'+ this.groupId +'/users'
    this.userService.fetchUserList(url)
    // .map((res) => this.extractData(res))
    // .pipe(map(this.extractData))    
    .subscribe((res) => {      
      this.rerender()
      this.userList = res.body
      console.log(JSON.stringify(res))
      // this.name = this.userList['firstName'] + ' ' + this.userList['middleName'] + ' ' + this.userList['lastName']
      this.isLoad = false
      this.extractData      
      this.dtTrigger.next();
    },
    (error) => {
      this.rerender()        
      this.isLoad = false
      this.noDataFound = 'No data found!'
      this.userList = []
      this.dtTrigger.next();
    }
  )
    }else{
      this.rerender()        
      this.isLoad = false
      this.noDataFound = 'No data found!'
      this.userList = []
      this.dtTrigger.next();
    }
    
    
       
  }

  rerender(): void {  
    this.dtElement.dtInstance.then((dtInstance: DataTables.Api) => {
      // Destroy the table first
      dtInstance.destroy();
      // Call the dtTrigger to rerender again
      // this.dtTrigger.next();
    });
  }
  
  // doSelectionChanges(options: INgxSelectOption[]) {
  //   this.selectedGroup = options;
  //   this.groupName = options[0].text
  //   console.log('doSelectionChanges', options);
  // }
  
  ngAfterViewInit(): void {
    this.dtTrigger.next();
  }

  ngOnDestroy(): void {  
    this.dtTrigger.unsubscribe();    
  }

  private extractData(res: Response) {
    const body = res.body;
    console.log('extra body data '  + body)
    console.log('extra body data sadfasdf ')
    return body || {};
  }

}
