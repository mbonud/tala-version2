import { Component, OnInit } from '@angular/core';
import { Subject } from 'rxjs';
import { RankService } from '@app/shared/services/rank.service'
import { AddModalRankComponent } from '@app/shared/modal/rank/add-modal-rank/add-modal-rank.component';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { ToastContainerDirective, ToastrService } from 'ngx-toastr';
import { SessionService } from '@app/core/session/session.service';
import { EditModalRankComponent } from '@app/shared/modal/rank/edit-modal-rank/edit-modal-rank.component';
import { DeleteModalRankComponent } from '@app/shared/modal/rank/delete-modal-rank/delete-modal-rank.component';

@Component({
  selector: 'app-rank',
  templateUrl: './rank.component.html',
  styleUrls: ['./rank.component.css']
})
export class RankComponent implements OnInit {
  urlRank = 'ranks?limit=500'  
  rankList = []
  isLoad = false;
  modalRef: BsModalRef;    
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();    
  noDataFound = 'No data found!'
  config = {
    backdrop: true,
    ignoreBackdropClick: true
  };

  constructor(
    private rankService: RankService,
    private sessionService: SessionService,
    private modalService: BsModalService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.isLoad = true
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,      
    };    
    this.fetchRankList()
  }

  fetchRankList(){
    let url = this.urlRank
    this.rankService.fetchRankList(url)
    .subscribe((res) => {
      console.log(res.body.ranks)
      this.dtOptions = {
        pagingType: 'full_numbers',
        pageLength: 5,         
        destroy: true,
        retrieve:true,
        paging: false        
      };
      this.rankList = res.body.ranks
      this.isLoad = false
      this.extractData
      this.dtTrigger.next();
    },
    (error) => {
      this.isLoad = false
      this.noDataFound = 'No data found!'
      this.rankList = []
    }
    )
  }
  

  private extractData(res: Response) {
    const body = res.body;
    return body || {};
  }

  addRankModal(){
    this.modalRef = this.modalService.show(AddModalRankComponent, this.config);
    
    (<AddModalRankComponent>this.modalRef.content).onClose.subscribe(result => {
      if (result === true) {        
        this.toastr.success('Success!');
        this.fetchRankList()
      }
    });
  }

  openEditRankDialog(rankId: string){
    const initialState = {
      data : { rankId: rankId },
      title: 'Edit Group details.'
    }     
    this.modalRef = this.modalService.show(
      EditModalRankComponent,
      Object.assign({initialState}, { class: 'modal-dialog' }, this.config)
    );    

    (<EditModalRankComponent>this.modalRef.content).onClose.subscribe(result => {
      if (result === true) {        
        this.toastr.success('Success!');
        this.fetchRankList()
      }
    });
  }

  openDeleteRank(rankId: string, rankName: string){
    const initialState = {
      data : { rankId: rankId },
      title: 'Do you want to delete this ' + rankName + '?'
    }     
    this.modalRef = this.modalService.show(
      DeleteModalRankComponent,
      Object.assign({initialState}, { class: 'modal-dialog' }, this.config)
    );    

    (<DeleteModalRankComponent>this.modalRef.content).onClose.subscribe(result => {
      if (result === true) {        
        this.toastr.success('Success!');
        this.fetchRankList()
      }
    });
  }

}
