import { Component, OnInit } from '@angular/core';
import { PrincipalService } from '@app/shared/services/principal.service'
import { SessionService } from '@app/core/session/session.service';
import { Subject } from 'rxjs';
import { PrincpalList,Principal } from '@app/models/principal.model';
import { AddModalGroupComponent } from '@app/shared/modal/group/add-modal-group/add-modal-group.component';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { ToastContainerDirective, ToastrService } from 'ngx-toastr';
import { EditModalGroupComponent } from '@app/shared/modal/group/edit-modal-group/edit-modal-group.component';
import { DeleteModalGroupComponent } from '@app/shared/modal/group/delete-modal-group/delete-modal-group.component';

@Component({
  selector: 'app-group',
  templateUrl: './group.component.html',
  styleUrls: ['./group.component.css']
})
export class GroupComponent implements OnInit {
  userdata = this.sessionService.getValue('user');
  isLoad = false;
  modalRef: BsModalRef;    
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();  
  principalList = [];
  noDataFound = 'No data found!'
  config = {
    backdrop: true,
    ignoreBackdropClick: true
  };

  constructor(
    private principalService: PrincipalService,
    private sessionService: SessionService,
    private modalService: BsModalService,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.isLoad = true;
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,      
    };    
    this.loadPrincipalData()
  }

  addGroupModal(){
    this.modalRef = this.modalService.show(AddModalGroupComponent, this.config );
    
    (<AddModalGroupComponent>this.modalRef.content).onClose.subscribe(result => {
      if (result === true) {   
        this.toastr.success('Success!');
        this.loadPrincipalData()
      }
    });
  }

  openEditGroupDialog(groupId: string){
    const initialState = {
      data : { groupId: groupId },
      title: 'Edit Group details.'
    }     
    this.modalRef = this.modalService.show(
      EditModalGroupComponent,
      Object.assign({initialState}, { class: 'modal-dialog' }, this.config)
    );    

    (<EditModalGroupComponent>this.modalRef.content).onClose.subscribe(result => {
      if (result === true) {        
        this.toastr.success('Success!');
        this.loadPrincipalData()
      }
    });
  }

  openDeleteGroup(groupId: string, groupName: string){
    const initialState = {
      data : { groupId: groupId },
      title: 'Do you want to delete this ' + groupName + '?'
    }     
    this.modalRef = this.modalService.show(
      DeleteModalGroupComponent,
      Object.assign({initialState}, { class: 'modal-dialog' }, this.config)
    );    

    (<DeleteModalGroupComponent>this.modalRef.content).onClose.subscribe(result => {
      if (result === true) {        
        this.toastr.success('Success!');
        this.loadPrincipalData()
      }
    });
  }

  loadPrincipalData(){
    this.principalService.fetchPrincialList('groups')
    .subscribe((res) => {
      console.log(res.body)
        this.dtOptions = {
          pagingType: 'full_numbers',
          pageLength: 5,         
          destroy: true,
          retrieve:true,
          paging: false        
        };
        this.principalList = res.body.groups
        this.isLoad = false       
        this.extractData
        this.dtTrigger.next();
    },
    (error) => {      
      this.isLoad = false
      this.noDataFound = 'No data found!'
      this.principalList = []
    }
    )    
  }

  private extractData(res: Response) {
    const body = res.body;
    return body || {};
  }

}
