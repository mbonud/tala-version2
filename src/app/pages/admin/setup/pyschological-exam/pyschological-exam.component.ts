import { Component, OnInit } from '@angular/core';
import { PsychologyService } from '@app/shared/services/psychology.service';
import { BsModalService, BsModalRef, ModalOptions } from 'ngx-bootstrap/modal';
import { Subject } from 'rxjs';

@Component({
  selector: 'app-pyschological-exam',
  templateUrl: './pyschological-exam.component.html',
  styleUrls: ['./pyschological-exam.component.css']
})
export class PyschologicalExamComponent implements OnInit {
  urlExam = 'auth/psych/exams'
  psycohlogicalList = []
  isLoad = false;
  modalRef: BsModalRef;    
  dtOptions: DataTables.Settings = {};
  dtTrigger: Subject<any> = new Subject();    
  noDataFound = 'No data found!'
  constructor(
    private psychologyService: PsychologyService
  ) { }

  ngOnInit(): void {
    this.isLoad = true
    this.dtOptions = {
      pagingType: 'full_numbers',
      pageLength: 5,      
    };    
    this.fetchExam(this.urlExam)
  }

  fetchExam(url: string){
    this.psychologyService.fetchPyschologyList(url)
    .subscribe((res) => {
      console.log(res.body.exams)
      this.psycohlogicalList = res.body.exams
      this.isLoad = false
      this.extractData
      this.dtTrigger.next();
    },
    (error) => {
      this.isLoad = false
      this.noDataFound = 'No data found!'
      this.psycohlogicalList = []
    }
    )
  }

  private extractData(res: Response) {
    const body = res.body;
    return body || {};
  }

  openEditDialog(id: string){
    
  }

}
