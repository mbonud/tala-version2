import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { SetupRoutingModule } from './setup-routing.module';
import { SetupComponent } from './setup.component';
import { GroupComponent } from './group/group.component';
import { PyschologicalExamComponent } from './pyschological-exam/pyschological-exam.component';
import { RankComponent } from './rank/rank.component';
import { UserComponent } from './user/user.component';
import { DataTablesModule } from 'angular-datatables';
import { TabsModule } from 'ngx-bootstrap/tabs';
import { UserListComponent } from './user/user-list/user-list.component';
import { PsychologicalTestExamComponent } from './user/psychological-test-exam/psychological-test-exam.component';
import { ForApprovalComponent } from './user/for-approval/for-approval.component';
import { NgxSelectModule } from 'ngx-select-ex';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

@NgModule({
  declarations: [SetupComponent, GroupComponent, PyschologicalExamComponent, RankComponent, UserComponent, UserListComponent, PsychologicalTestExamComponent, ForApprovalComponent],
  imports: [
    CommonModule,
    SetupRoutingModule,
    DataTablesModule,
    TabsModule.forRoot(),
    NgxSelectModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class SetupModule { }
