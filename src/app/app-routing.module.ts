import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
// import { AppComponent } from './app.component';
// import { LoginComponent } from './accounts/login/login.component';
// import { AccountsComponent } from './accounts/accounts.component';
// import { PageNotFoundComponent } from './page-not-found/page-not-found.component';


const routes: Routes = [  
  { path: '', redirectTo: 'accounts/login', pathMatch: 'full' },
  { 
    path: 'accounts',
    loadChildren: () => import('./accounts/accounts.module').then(m => m.AccountsModule)
  },  
  // { path: 'not-found', component: PageNotFoundComponent },
  // { path: '**', redirectTo: '/not-found' }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})

export class AppRoutingModule { }
