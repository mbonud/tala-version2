import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpErrorResponse, HttpHeaders, HttpEventType, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Course, CourseBody } from '@app/models/course.model';
import { Quizzes, QuizBody } from '@app/models/quizzes.model';
import { FlashCard, FlashCardBody } from '@app/models/flash-card.model';


@Injectable({
  providedIn: 'root'
})
export class LibraryService {

  constructor(
  	private httpClient: HttpClient
  ) { }

  	assignUser(url: string, data): Observable<HttpResponse<any>> {
		return this.httpClient
		.enableAuth()
		.post<any>(url, data, { observe: 'response' })
		.pipe(
			map((response) => response),
			catchError((error: HttpErrorResponse) => throwError(error))	
		);
	}

  	fetchLibaryDetails(url: string): Observable<HttpResponse<any>> {
		return this.httpClient		
			.get<any>(url, { observe: 'response' })
			.pipe(
				catchError((error: HttpErrorResponse) => throwError(error))
			);
	}		

	uploadFile(fileUpload: any, url: string): Observable<HttpResponse<any>> {
		return this.httpClient
			.post<any>(url, fileUpload, {
				reportProgress: true,				
				observe: 'response' })
			.pipe(
				map((response) => response),
				catchError((error: HttpErrorResponse) => throwError(error))
			);
	}

	putFile(url: string, file: any, contentType: string){
		return this.httpClient
			.disableApiPrefix()
			.put(url, file, { headers: new HttpHeaders({ 'Content-Type': contentType, 'Cache-Control' : 'no-cache'}),
			reportProgress: true,	 observe: 'events' })
			// .pipe(
			// 	catchError((error: HttpErrorResponse) => throwError(error))
			// );
			.pipe(map((event) => {

				switch (event.type) {
		  
				  case HttpEventType.UploadProgress:
					const progress = Math.round(100 * event.loaded / event.total);
					return { status: 'progress', message: progress };
		  
				  case HttpEventType.Response:
					return event.body;
				  default:
					return `Unhandled event: ${event.type}`;
				}
			  })
			);
	}

	//course crud
	fetchCourseDetails(url: string): Observable<HttpResponse<Course>> {
		return this.httpClient
			.get<Course>(url, { observe: 'response' })
			.pipe(
				catchError((error: HttpErrorResponse) => throwError(error))
			);
	}

	uploadCourseData(dataForm: CourseBody): Observable<HttpResponse<CourseBody>> {
		return this.httpClient
			.enableAuth()
			.post<CourseBody>(`auth/courses`, dataForm, { 
			// .post<Course>(`courses`, dataForm, { 
				reportProgress: true,
				observe: 'response' })
			.pipe(
				map((response) => response),
				// catchError((error: HttpErrorResponse) => throwError(error))	
			);
	}
	
	enableCourseData(itemType: string, dataForm: any, itemId: string): Observable<HttpResponse<any>> {
		return this.httpClient
			.enableAuth()
			// .put<Quizzes>(`auth/${itemType}` + '/' + itemId, dataForm, { observe: 'response' })
			.put<any>(`${itemType}` + '/' + itemId, dataForm, { observe: 'response' })
			.pipe(
				map((response) => response),
				catchError((error: HttpErrorResponse) => throwError(error))	
			);
	}

	updateCourseData(dataForm: CourseBody, courseId: string): Observable<HttpResponse<CourseBody>> {
		return this.httpClient
			.enableAuth()
			.put<CourseBody>(`auth/courses` + '/' + courseId, dataForm, { observe: 'response' })
			// .put<Course>(`courses` + '/' + courseId, dataForm, { observe: 'response' })
			.pipe(
				map((response) => response),
				catchError((error: HttpErrorResponse) => throwError(error))	
			);
	}

	//flashcard crud
	fetchFlashCardDetails(url: string): Observable<HttpResponse<FlashCard>> {
		return this.httpClient		
			.get<FlashCard>(url, { observe: 'response' })
			.pipe(
				catchError((error: HttpErrorResponse) => throwError(error))
			);
	}

	uploadFlashcardData(dataForm: FlashCardBody): Observable<HttpResponse<FlashCardBody>> {
		return this.httpClient
			.enableAuth()
			.post<FlashCardBody>(`auth/flashcards`, dataForm, { observe: 'response' })
			// .post<FlashCardBody>(`flashcards`, dataForm, { observe: 'response' })
			.pipe(
				map((response) => response),
				catchError((error: HttpErrorResponse) => throwError(error))	
			);
	}

	updateFlashcardData(dataForm: FlashCardBody, cardId: string): Observable<HttpResponse<FlashCardBody>> {
		return this.httpClient
			.enableAuth()
			.put<FlashCardBody>(`auth/flashcards` + '/' + cardId, dataForm, { observe: 'response' })
			// .put<FlashCardBody>(`flashcards` + '/' + cardId, dataForm, { observe: 'response' })
			.pipe(
				map((response) => response),
				catchError((error: HttpErrorResponse) => throwError(error))	
			);
	}

	// crud quiz
	fetchQuizDetails(url: string): Observable<HttpResponse<Quizzes>> {
		return this.httpClient		
			.get<Quizzes>(url, { observe: 'response' })
			.pipe(
				catchError((error: HttpErrorResponse) => throwError(error))
			);
	}

	uploadQuizData(dataForm: QuizBody): Observable<HttpResponse<QuizBody>> {
		return this.httpClient
			.enableAuth()
			.post<QuizBody>(`auth/quizzes`, dataForm, { observe: 'response' })
			// .post<QuizBody>(`quizzes`, dataForm, { observe: 'response' })
			.pipe(
				map((response) => response),
				catchError((error: HttpErrorResponse) => throwError(error))	
			);
	}

	updateQuizData(dataForm: QuizBody, quizId: string): Observable<HttpResponse<QuizBody>> {
		return this.httpClient
			.enableAuth()
			.put<QuizBody>(`auth/quizzes` + '/' + quizId, dataForm, { observe: 'response' })
			// .put<QuizBody>(`quizzes` + '/' + quizId, dataForm, { observe: 'response' })
			.pipe(
				map((response) => response),
				catchError((error: HttpErrorResponse) => throwError(error))	
			);
	}

	generateItemId(itemType: string){
		return this.httpClient
		.enableAuth()
		// .get<any>(`auth/${itemType}/id`, { observe: 'response' })
		.get<any>(`${itemType}/id`, { observe: 'response' })
		.pipe(
			catchError((error: HttpErrorResponse) => throwError(error))
		)
	}

	//disable file from list of library courses/flashcard/quiz
	disableData(itemType: string, itemId: string) {
		return this.httpClient
			.enableAuth()
			.delete(`auth/${itemType}` + '/' + itemId, { observe: 'response' })
			// .delete(`${itemType}` + '/' + itemId, { observe: 'response' })
			.pipe(
				map((response) => response),
				catchError((error: HttpErrorResponse) => throwError(error))	
			);
  }
  
  enableData(itemType: string, dataForm: any, itemId: string): Observable<HttpResponse<Quizzes>> {
		return this.httpClient
			.enableAuth()
			.put<Quizzes>(`auth/${itemType}` + '/' + itemId, dataForm, { observe: 'response' })
			// .put<Quizzes>(`${itemType}` + '/' + itemId, dataForm, { observe: 'response' })
			.pipe(
				map((response) => response),
				catchError((error: HttpErrorResponse) => throwError(error))	
			);
	}

	// fetch library dashboard report
	fetchDashboardReports(url: string): Observable<HttpResponse<any>> {
		return this.httpClient
		.disableApiPrefix()
		.enableAuth()
		.get<any>(url, { observe: 'response', responseType: 'json'})
		.pipe(
		  catchError((error: HttpErrorResponse) => throwError(error))
		)
	  }
	
}
