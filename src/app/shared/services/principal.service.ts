import { Injectable } from '@angular/core';
import { Observable, throwError, from  } from 'rxjs';
import { HttpResponse, HttpErrorResponse, HttpClient } from '@angular/common/http';
import { catchError, map, mergeMap } from 'rxjs/operators';
import { Principal, PostPrincipal, Principals, PrincpalList } from '@app/models/principal.model';

@Injectable({
  providedIn: 'root'
})
export class PrincipalService {

  constructor(
    private httpClient: HttpClient,
  ) { }

  // fethc or get list of principal
  fetchPrincialList(url: string): Observable<HttpResponse<PrincpalList>> {
    return this.httpClient
      .enableAuth()		
      .get<PrincpalList>(url, { observe: 'response' })
      .pipe(
        catchError((error: HttpErrorResponse) => throwError(error))
      );
    }

    //add principal
    AddPrincipal(dataForm: PostPrincipal): Observable<HttpResponse<PostPrincipal>> {
      return this.httpClient
        .enableAuth()
        .post<PostPrincipal>(`auth/groups`, dataForm, { observe: 'response' })
        // .post<PostPrincipal>(`groups`, dataForm, { observe: 'response' })
        .pipe(
          map((response) => response),
          catchError((error: HttpErrorResponse) => throwError(error))	
        );
      }

    // get principal details by id
    fetchPrincipalData(groupId: string): Observable<HttpResponse<PostPrincipal>> {
      return this.httpClient		
        .get<PostPrincipal>('groups/'+ groupId, { observe: 'response' })
        .pipe(
          catchError((error: HttpErrorResponse) => throwError(error))
        );
    }

    // update principal
    updatePrincipalData(dataForm: PostPrincipal, groupId: string): Observable<HttpResponse<PostPrincipal>> {
      return this.httpClient
        .enableAuth()
        .put<PostPrincipal>(`auth/groups/${groupId}`, dataForm, { observe: 'response' })
        // .put<PostPrincipal>(`groups/${groupId}`, dataForm, { observe: 'response' })
        .pipe(
          map((response) => response),
          catchError((error: HttpErrorResponse) => throwError(error))	
        );
    }

    //remove principal
    deletePrincipal(groupId: string) {
      return this.httpClient
        .enableAuth()
        .delete(`auth/groups` + '/' + groupId, { observe: 'response' })
        // .delete(`groups` + '/' + groupId, { observe: 'response' })
        .pipe(
          map((response) => response),
          catchError((error: HttpErrorResponse) => throwError(error))	
        );
    }
}
