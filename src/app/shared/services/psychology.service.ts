import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpResponse, HttpHeaders } from '@angular/common/http';
import { catchError, map } from 'rxjs/operators';
import { Psychological, PsychoTestList } from '@app/models/psycho.model';
import { Observable, throwError } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PsychologyService {

  constructor(
    private httpClient: HttpClient
  ) { }

  fetchPyschologyList(url: string): Observable<HttpResponse<PsychoTestList>>{
    return this.httpClient
			.enableAuth()					
			.get<PsychoTestList>(url, { observe: 'response' })			
			.pipe(
				catchError((error: HttpErrorResponse) => throwError(error))
			);
  }

}
