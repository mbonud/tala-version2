import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpErrorResponse, HttpHeaders, HttpEventType, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { Course, CourseBody } from '@app/models/course.model';
import { Quizzes, QuizBody } from '@app/models/quizzes.model';
import { FlashCard, FlashCardBody } from '@app/models/flash-card.model';

@Injectable({
  providedIn: 'root'
})
export class DashboardService {

  constructor(
    private httpClient: HttpClient
  ) { }


  getCourseReports(url: string): Observable<HttpResponse<any>> {
    return this.httpClient
    .disableApiPrefix()
    .enableAuth()
    .get<any>(url, { observe: 'response', responseType: 'json'})
    .pipe(
      catchError((error: HttpErrorResponse) => throwError(error))
    )
  }


}
