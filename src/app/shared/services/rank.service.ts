import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Rank } from '@app/models/rank.model';
import { map, catchError } from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RankService {

  constructor(
    private httpClient: HttpClient
  ) { }

	fetchRankList(url: string): Observable<HttpResponse<Rank>> {
		return this.httpClient
			.enableAuth()		
			.get<Rank>(url, { observe: 'response' })
			.pipe(
				catchError((error: HttpErrorResponse) => throwError(error))
			);
	}

	addRank(dataForm: Rank): Observable<HttpResponse<Rank>> {
		return this.httpClient
			 .enableAuth()
			.post<Rank>(`auth/ranks`, dataForm, { observe: 'response' })
			// .post<Rank>(`ranks`, dataForm, { observe: 'response' })
			.pipe(
				map((response) => response),
				catchError((error: HttpErrorResponse) => throwError(error))	
			);
	  }
	
	  FetchRankData(rankId: string): Observable<HttpResponse<Rank>>{
		return this.httpClient
		// .enableAuth()
		.get<Rank>(`ranks/` + rankId, { observe: 'response' })
		.pipe(
			catchError((error: HttpErrorResponse) => throwError(error))
		);
  	}

	  updateRankData(dataForm: Rank, rankId: string): Observable<HttpResponse<Rank>> {
		return this.httpClient
			.enableAuth()
			.put<Rank>(`auth/ranks` + '/' + rankId, dataForm, { observe: 'response' })
			// .put<Rank>(`ranks` + '/' + rankId, dataForm, { observe: 'response' })
			.pipe(
				map((response) => response),
				catchError((error: HttpErrorResponse) => throwError(error))	
			);
	  }
	  
	  deleteRankData(rankId: string) {
		return this.httpClient
			.enableAuth()
			.delete(`auth/ranks` + '/' + rankId, { observe: 'response' })
			// .delete(`ranks` + '/' + rankId, { observe: 'response' })
			.pipe(
				map((response) => response),
				catchError((error: HttpErrorResponse) => throwError(error))	
			);
	}
  
}
