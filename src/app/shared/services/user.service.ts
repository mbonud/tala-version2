import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpErrorResponse, HttpEventType, HttpEvent, HttpHeaders } from '@angular/common/http';
import { User, Users, PsychUsers, NewUser } from '@app/models/user.model'
import { Observable, throwError } from 'rxjs';
import { map, catchError } from 'rxjs/operators';
import { AuthService } from '@app/core/auth/auth.service';
import { Tokens } from '@app/models/tokens.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  constructor(
    private httpClient: HttpClient,
	  private authService: AuthService
  ) { }

  addNewUser(dataForm: NewUser): Observable<HttpResponse<NewUser>> {
		return this.httpClient
			.post<User>(`users`, dataForm, { observe: 'response' })
			.pipe(
				map((response) => response),
				catchError((error: HttpErrorResponse) => throwError(error))	
			);
	}

  fetchUserList(url: string): Observable<HttpResponse<Users>> {
		return this.httpClient
      .enableAuth()
      .get<Users>('auth/' + url, { observe: 'response' })
      // .get<Users>( url, { observe: 'response' })
			.pipe(
        map(res => res),
				catchError((error: HttpErrorResponse) => throwError(error))
      )      
    } 

    fetchUserListFlashCard(url: string): Observable<HttpResponse<Users>> {
      return this.httpClient
        .enableAuth()
        // .get<Users>('auth/' + url, { observe: 'response' })
        .get<Users>( url, { observe: 'response' })
        .pipe(
          map(res => res),
          catchError((error: HttpErrorResponse) => throwError(error))
        )      
      } 

    fetchSeafarerList(url: string): Observable<HttpResponse<Users>> {
      return this.httpClient
        .enableAuth()
        // .get<Users>('auth/' + url, { observe: 'response' })
        .get<Users>( url, { observe: 'response' })
        .pipe(
          map(res => res),
          catchError((error: HttpErrorResponse) => throwError(error))
        )      
      } 

  
    fetchUserListEligible(url: string): Observable<HttpResponse<any>> {
      return this.httpClient
        .enableAuth()		
        .get<any>(url, { observe: 'response' })		
        .pipe(
          catchError((error: HttpErrorResponse) => throwError(error))
        );
      }

      fetchUserData(userId: string): Observable<HttpResponse<User>>{
        return this.httpClient
        .enableAuth()        
        .get<User>(`users/` + userId, { observe: 'response' })
        .pipe(
          catchError((error: HttpErrorResponse) => throwError(error))
        );
      }

      updateUserData(dataForm: User, userId: string): Observable<HttpResponse<User>> {
        return this.httpClient
          .enableAuth()
          .put<User>(`auth/users/${userId}`, dataForm, { observe: 'response' })
          // .put<User>(`users/${userId}`, dataForm, { observe: 'response' })              
          .pipe(
            map((response) => response),
            catchError((error: HttpErrorResponse) => throwError(error))	
          );
      }

      deleteUserData(userId: string) {
        return this.httpClient
          .enableAuth()
          // .delete(`auth/users/` + userId, { observe: 'response' })
          .delete(`users/` + userId, { observe: 'response' })
          .pipe(
            map((response) => response),
            catchError((error: HttpErrorResponse) => throwError(error))	
          );
      }

}
