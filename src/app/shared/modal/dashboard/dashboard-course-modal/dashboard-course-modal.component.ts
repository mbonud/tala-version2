import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Subject, ReplaySubject } from 'rxjs';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { DashboardService } from '@app/shared/services/dashboard.service';
import { StarRatingComponent } from 'ng-starrating';

@Component({
  selector: 'app-dashboard-course-modal',
  templateUrl: './dashboard-course-modal.component.html',
  styleUrls: ['./dashboard-course-modal.component.css']
})
export class DashboardCourseModalComponent implements OnInit {
  data:  []
  dtOptionsAssign: DataTables.Settings = {};
  dtTriggerAssign: Subject<any> = new Subject();  
  public onClose: Subject<boolean>;
  public active: boolean = false;  
  noDataFound = 'No data found!'
  isLoad = false;
  courseList;
  constructor(
    public bsModalRef: BsModalRef,    
    private toastr: ToastrService,
    private dashboardService: DashboardService
  ) { 
    
  }

  ngOnInit(): void {
    this.getCourseReportDetails(this.data['groupId'], this.data['coursesId'])
    this.onClose = new Subject();
    this.dtOptionsAssign = {
      pagingType: 'full_numbers',
      pageLength: 5,
    };
  }

  onRate($event:{oldValue:number, newValue:number, starRating:StarRatingComponent}) {
    alert(`Old Value:${$event.oldValue}, 
      New Value: ${$event.newValue}, 
      Checked Color: ${$event.starRating.checkedcolor}, 
      Unchecked Color: ${$event.starRating.uncheckedcolor}`);
  }

  getCourseReportDetails(groupId: string, urlId: string){ 
    this.dashboardService.getCourseReports(`http://tala-maritime.ap-northeast-1.elasticbeanstalk.com/api/v1/auth/dashboard/groups/`+ groupId +`/courses/`+ urlId +`/reports`)
    .subscribe(
      (res) => {
        this.dtOptionsAssign = {
          pagingType: 'full_numbers',
          pageLength: 5,         
          destroy: true,
          retrieve:true,
          paging: false        
        };
        this.courseList = res.body.userList    
        console.log( 'user list ' + JSON.stringify(res.body))        
        this.isLoad = false
        this.extractData
        this.dtTriggerAssign.next();
      },
      (error) => {
        this.isLoad = false
        this.noDataFound = 'No data found!'
      }                      
    )
  }

  public onCancel(): void {
    this.active = false;
    this.onClose.next(false);
    this.bsModalRef.hide();
}
  onConfirm(){    
  
  }

  private extractData(res: Response) {
    const body = res.body;
    return body || {};
  }

}
