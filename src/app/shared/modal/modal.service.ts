import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpErrorResponse, HttpHeaders, HttpEventType, HttpParams } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { Course, CourseList } from '@app/models/course.model';
import { catchError, map } from 'rxjs/operators';
import { FlashCard } from '@app/models/flash-card.model';
import { Constants } from '@app/core/utils';
import { Psychological } from '@app/models/psycho.model';
import { Quizzes, Quizzzesss, QuizList } from '@app/models/quizzes.model';

@Injectable({
  providedIn: 'root'
})
export class ModalService {

  constructor(
  	private httpClient: HttpClient
  ) { }

  //disable file from list of library courses/flashcard/quiz
  disableData(itemType: string, itemId: string) {
		return this.httpClient
			.enableAuth()
			// .delete(`auth/${itemType}` + '/' + itemId, { observe: 'response' })
			.delete(`${itemType}` + '/' + itemId, { observe: 'response' })
			.pipe(
				map((response) => response),
				catchError((error: HttpErrorResponse) => throwError(error))	
			);
  }
  
  enableData(itemType: string, dataForm: any, itemId: string): Observable<HttpResponse<any>> {
		return this.httpClient
			.enableAuth()
			// .put<Quizzes>(`auth/${itemType}` + '/' + itemId, dataForm, { observe: 'response' })
			.put<any>(`${itemType}` + '/' + itemId, dataForm, { observe: 'response' })
			.pipe(
				map((response) => response),
				catchError((error: HttpErrorResponse) => throwError(error))	
			);
	}	
}
