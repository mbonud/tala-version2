import { Component, OnInit } from '@angular/core';
import { Subject, ReplaySubject } from 'rxjs';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgForm, FormControl } from '@angular/forms';
import { RankService } from '@app/shared/services/rank.service';

@Component({
  selector: 'app-delete-modal-rank',
  templateUrl: './delete-modal-rank.component.html',
  styleUrls: ['./delete-modal-rank.component.css']
})
export class DeleteModalRankComponent implements OnInit {
  public onClose: Subject<boolean>;
  public active: boolean = false;
  data: []
  rankId;
  title;
  
  constructor(
    public bsModalRef: BsModalRef,
    private rankService: RankService
  ) { }

  ngOnInit(): void {
    this.onClose = new Subject();
    this.rankId = this.data['rankId']
  }

  public onCancel(): void {
    this.active = false;
    this.onClose.next(false);
    this.bsModalRef.hide();
  }

  onConfirm(): void {   
    this.rankService.deleteRankData(this.rankId)
    .subscribe((res) => {
      console.log(res)
      this.active = false;
      this.onClose.next(true);
      this.bsModalRef.hide();
    })
  }

}
