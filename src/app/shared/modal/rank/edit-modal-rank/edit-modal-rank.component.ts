import { Component, OnInit } from '@angular/core';
import { Subject, ReplaySubject } from 'rxjs';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgForm, FormControl } from '@angular/forms';
import { RankService } from '@app/shared/services/rank.service'
import { Rank } from '@app/models/rank.model';
@Component({
  selector: 'app-edit-modal-rank',
  templateUrl: './edit-modal-rank.component.html',
  styleUrls: ['./edit-modal-rank.component.css']
})
export class EditModalRankComponent implements OnInit {
  public onClose: Subject<boolean>;
  public active: boolean = false;
  data: []
  rankId;
  rankData = {
    rank: ''
  };
  constructor(
    public bsModalRef: BsModalRef,
    private rankService: RankService
  ) { }

  ngOnInit(): void {
    this.onClose = new Subject();
    this.rankId = this.data['rankId']
    this.fetchRankDetails(this.rankId)
  }

  fetchRankDetails(rankId){
    this.rankService.FetchRankData(rankId)
    .subscribe((res) => {
      this.rankData = res.body
      console.log(res)
      console.log(res.body)
    })
  }

  public onCancel(): void {
    this.active = false;
    this.onClose.next(false);
    this.bsModalRef.hide();
  }

  onSubmit(formValue : NgForm): void {    
    const data: Rank  = formValue.value;  
    this.rankService.updateRankData(data, this.rankId)
    .subscribe((res) => {
      console.log(res)
      this.active = false;
      this.onClose.next(true);
      this.bsModalRef.hide();
    })
  }

}
