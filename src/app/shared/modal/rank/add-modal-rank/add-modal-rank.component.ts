import { Component, OnInit } from '@angular/core';
import { Subject, ReplaySubject } from 'rxjs';
import { NgForm, FormControl } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { RankService } from '@app/shared/services/rank.service'
import { Rank } from '@app/models/rank.model';

@Component({
  selector: 'app-add-modal-rank',
  templateUrl: './add-modal-rank.component.html',
  styleUrls: ['./add-modal-rank.component.css']
})
export class AddModalRankComponent implements OnInit {
  public onClose: Subject<boolean>;
  public active: boolean = false;

  constructor(
    private rankService: RankService,
    public bsModalRef: BsModalRef,
  ) { }

  ngOnInit(): void {
    this.onClose = new Subject();
  }

  public onCancel(): void {
    this.active = false;
    this.onClose.next(false);
    this.bsModalRef.hide();
  }

  // public onConfirm(): void {
  //   this.active = false;
  //   this.onClose.next(false);
  //   this.bsModalRef.hide();
  // }

  onSubmit(formValue : NgForm): void {
    console.log(formValue)
    const rank: Rank  = formValue.value;  
    this.rankService.addRank(rank)
    .subscribe((res) => {
      console.log(res)
      this.active = false;
      this.onClose.next(true);
      this.bsModalRef.hide();
    })
  }

}
