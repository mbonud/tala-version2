import { Component, OnInit } from '@angular/core';
import { Subject, ReplaySubject } from 'rxjs';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgForm, FormControl } from '@angular/forms';
import { PrincipalService } from '@app/shared/services/principal.service';
import { PostPrincipal } from '@app/models/principal.model';

@Component({
  selector: 'app-edit-modal-group',
  templateUrl: './edit-modal-group.component.html',
  styleUrls: ['./edit-modal-group.component.css']
})
export class EditModalGroupComponent implements OnInit {
  public onClose: Subject<boolean>;
  public active: boolean = false;
  data: []
  principalDetails ={
    groupName: '',
    groupAddress: '',
    groupEmail: '',
    contactPerson: '',
    contactNumber: '',
    groupCode: ''
  };  
  groupId;
  constructor(
    public bsModalRef: BsModalRef,   
    private principalService: PrincipalService, 
  ) { }

  ngOnInit(): void {
    this.onClose = new Subject();
    this.groupId = this.data['groupId']
    this.fetchPrincipalData(this.groupId)
  }

  fetchPrincipalData(groupId: string){
    this.principalService.fetchPrincipalData(groupId)
      .subscribe((res) => {
        this.principalDetails = res.body
        console.log(res)
        console.log(this.principalDetails)
      })
  }

  public onCancel(): void {
    this.active = false;
    this.onClose.next(false);
    this.bsModalRef.hide();
  }

  onSubmit(formValue : NgForm): void {
    let data: PostPrincipal = formValue.value;
    this.principalService.updatePrincipalData(data, this.groupId)
    .subscribe((res) => {
      console.log(res)
      this.active = false;
      this.onClose.next(true);
      this.bsModalRef.hide();
    })
  }

}
