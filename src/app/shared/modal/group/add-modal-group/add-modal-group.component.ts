import { Component, OnInit } from '@angular/core';
import { Subject, ReplaySubject } from 'rxjs';
import { NgForm, FormControl } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { PrincipalService } from '@app/shared/services/principal.service'
import { PostPrincipal } from '@app/models/principal.model';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-modal-group',
  templateUrl: './add-modal-group.component.html',
  styleUrls: ['./add-modal-group.component.css']
})
export class AddModalGroupComponent implements OnInit {
  //modal button
  public onClose: Subject<boolean>;
  public active: boolean = false;
  loader = false;
  model: any = {}
  constructor(
    private principalService: PrincipalService,
    public bsModalRef: BsModalRef,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this.onClose = new Subject();
  }

  public onCancel(): void {
    this.active = false;
    this.onClose.next(false);
    this.bsModalRef.hide();
  }

  onSubmit(formValue : NgForm): void {
    this.loader = true;
    let data: PostPrincipal = formValue.value;
    data.contactNumber = data.contactNumber.toString()
    this.principalService.AddPrincipal(data)
    .subscribe((res) => {
      this.loader = false;
      console.log(res)
      this.active = false;
      this.onClose.next(true);
      this.bsModalRef.hide();
    },
    (error) => {
      this.loader = false;
      this.toastr.error(JSON.stringify(Object.values(error.error)[0]));
    })
  }
}
