import { Component, OnInit } from '@angular/core';
import { Subject, ReplaySubject } from 'rxjs';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgForm, FormControl } from '@angular/forms';
import { PrincipalService } from '@app/shared/services/principal.service';

@Component({
  selector: 'app-delete-modal-group',
  templateUrl: './delete-modal-group.component.html',
  styleUrls: ['./delete-modal-group.component.css']
})
export class DeleteModalGroupComponent implements OnInit {
  public onClose: Subject<boolean>;
  public active: boolean = false;
  data: []
  groupId;
  title;
  constructor(
    public bsModalRef: BsModalRef,   
    private principalService: PrincipalService, 
  ) { }

  ngOnInit(): void {
    this.onClose = new Subject();
    this.groupId = this.data['groupId']
  }

  public onCancel(): void {
    this.active = false;
    this.onClose.next(false);
    this.bsModalRef.hide();
  }

  onConfirm(): void {   
    this.principalService.deletePrincipal(this.groupId)
    .subscribe((res) => {
      console.log(res)
      this.active = false;
      this.onClose.next(true);
      this.bsModalRef.hide();
    })
  }

}
