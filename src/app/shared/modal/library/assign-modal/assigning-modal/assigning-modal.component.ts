import { Component, OnInit } from '@angular/core';
import { Subject, ReplaySubject } from 'rxjs';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { UserService } from '@app/shared/services/user.service'
import { ToastrService } from 'ngx-toastr';
import { LibraryService } from '@app/shared/services/library.service';
import { NewUser } from '@app/models/user.model';

@Component({
  selector: 'app-assigning-modal',
  templateUrl: './assigning-modal.component.html',
  styleUrls: ['./assigning-modal.component.css']
})
export class AssigningModalComponent implements OnInit {
  public onClose: Subject<boolean>;
  public active: boolean = false;
  url: string;
  title: string;
  AssignedBody;
  assignItem: string;
  userName;
  needPayment;
  btnDisable = false

  constructor(
    public bsModalRef: BsModalRef,
    private modalService: BsModalService,
    private toastr: ToastrService,
    private libraryService: LibraryService,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    console.log(this.needPayment)
    this.onClose = new Subject();
    console.log(this.AssignedBody)
    this.userName = (this.AssignedBody['firstName'] + ' ' + this.AssignedBody['middleName'] + ' ' + this.AssignedBody['lastName']).toUpperCase()
  }

  public onCancel(): void {
    this.active = false;
    this.onClose.next(false);
    this.bsModalRef.hide();
  }

  getPayment(payment: number){
    console.log(payment)
    if(!payment){
      this.btnDisable = false
    }else {
      this.btnDisable = true
    }
    this.needPayment = payment
    console.log(this.btnDisable)
  }

  assignUser(): void {
    this.AssignedBody['needPayment'] = this.needPayment
    this.libraryService.assignUser(this.url, this.AssignedBody)
    .subscribe((res) => {
      console.log(res)
      this.active = false;
      this.onClose.next(true);
      this.bsModalRef.hide();
    },
    (error) => {
      console.log(error)
      console.log(JSON.stringify(error.error.errorMessage))
      this.toastr.error(error.error.errorMessage);
    })
  }

}
