import { Component, OnInit, OnDestroy } from '@angular/core';
import { Subject, ReplaySubject } from 'rxjs';
import { NgForm, FormControl } from '@angular/forms';
import { BsModalRef, BsModalService } from 'ngx-bootstrap/modal';
import { UserService } from '@app/shared/services/user.service'
import { ToastrService } from 'ngx-toastr';
import { LibraryService } from '@app/shared/services/library.service';
import { NewUser } from '@app/models/user.model';
import { AssigningModalComponent } from './assigning-modal/assigning-modal.component';

@Component({
  selector: 'app-assign-modal',
  templateUrl: './assign-modal.component.html',
  styleUrls: ['./assign-modal.component.css']
})
export class AssignModalComponent implements OnInit, OnDestroy {
  public onClose: Subject<boolean>;
  public active: boolean = false;
  data:  []
  title: string  
  itemType: string;
  itemId: string;
  assignItem: string;
  needPayment: string;
  isLoad = false;
  modalRef: BsModalRef;  
  config = {
    backdrop: true,
    ignoreBackdropClick: true
  };
  dtOptionsAssign: DataTables.Settings = {};
  dtTriggerAssign: Subject<any> = new Subject();  
  noDataFound = 'No data found!'
  url;
  userList;
  checkList = [];
  tempCheckList = [];
  isChecked = false;
  isCheckedAll = false
  group;
  AssignedBody: NewUser;

  constructor(
    public bsModalRef: BsModalRef,
    private modalService: BsModalService,
    private toastr: ToastrService,
    private libraryService: LibraryService,
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.onClose = new Subject();
    this.itemType = this.data['itemType']    
    this.itemId = this.data['itemId']
    this.needPayment = this.data['needPayment']
    this.assignItem = this.data['assignItem']
    this.group = JSON.parse(JSON.stringify(this.data['group']))    
    console.log('groupId = ' + this.group.groupId + ' itemId = ' + this.itemId + ' item name = ' + this.itemType )

    this.dtOptionsAssign = {
      pagingType: 'full_numbers',
      pageLength: 5,
    };
    // this.url = 'admin/groups/1aa9673dfd40e253942d6f753397813e49ef3658/courses/926ff46e23bb922dfc8fc7b6d542c0810bd72dc6'
    this.url = 'admin/groups/'+ this.group.groupId +'/users'
    this.loadUserList(this.url)
  }

  public onCancel(): void {
    this.dtTriggerAssign.unsubscribe();
    this.active = false;
    this.onClose.next(false);
    this.bsModalRef.hide();
  }

  ngOnDestroy(): void { 
    this.dtTriggerAssign.unsubscribe();    
  }
  
  getCheckUser(event: any){  
    if(event.target.checked){
      console.log('user id ' + event.target.value)
      this.checkList.push({'userId':  event.target.value})
    } else {
      this.isCheckedAll = false
      let removeIndex = this.checkList.map(function(item) { return item.userId; }).indexOf(event.target.value);      
      console.log('remove index ' + removeIndex)
      this.checkList.splice(removeIndex, 1);  
    }
    console.log(this.checkList)
  }

   checkuncheckall(event: any){
     
     console.log(event.target.checked)
     this.isCheckedAll = event.target.checked
    if(event.target.checked){
      this.checkList = this.tempCheckList
      this.isChecked = event.target.checked;
    } else {
      this.checkList = []
      this.isChecked = event.target.checked;
    }  
  }
  openAssigningModal(userId: string){
    console.log(userId)
    // console.log('checklist ' + JSON.parse(this.tempCheckList))
    console.log('checklist2 ' + JSON.stringify(this.userList))
    console.log('checklist3 ' + this.userList)
    this.AssignedBody = this.userList.find(user => user.userId === userId)
    
    // console.log('assigned body ' + JSON.stringify(this.AssignedBody) + ' userId = ' + userId)
    console.log('assignBody ' + JSON.stringify(this.AssignedBody))
    
    const url = `admin/groups/${this.group.groupId}/${this.itemType}/${this.itemId}/users/${userId}` 
    console.log('url assign ' + url)
    const initialState = {
        url: url,
        title: 'Assign User',
        needPayment: this.needPayment,
        assignItem: this.assignItem.toUpperCase(),
        AssignedBody: this.AssignedBody      
      }
      this.modalRef = this.modalService.show(
        AssigningModalComponent,
        Object.assign({initialState},{ class: 'backDropModal' }, this.config)
        
      );    
  
      (<AssigningModalComponent>this.modalRef.content).onClose.subscribe(result => {
        if (result === true) {        
          this.toastr.success('Success!');
        }
      });    
  }

  onSubmitAssignedUser(){
    if(this.isCheckedAll){
      console.log('check all active')
      this.checkList = this.tempCheckList
    } 
    console.log(this.checkList)
    for(let user of JSON.parse(JSON.stringify(this.checkList))){
      const userId = user.userId
      const url = `admin/groups/${this.group.groupId}/courses/${this.itemId}/users/${userId}` 
      console.log('url ' + url)
        let assignBody = {
          userId: userId, 
          needPayment: this.needPayment, 
          groupId: this.group.groupId,
          groupName: this.group.groupName, 
          name: user.firstName + ' ' + user.lastName, 
          rank: user.rank, 
          email: user.email,
          mobile: user.mobile
        }

      this.AssignedBody = this.userList.find(user => user.userId === userId)
      // console.log('assigned body ' + JSON.stringify(this.AssignedBody) + ' userId = ' + userId)
      console.log('assignBody ' + JSON.stringify(assignBody))
      // this.libraryService.assignUser(url, JSON.stringify(this.AssignedBody))
      this.libraryService.assignUser(url, assignBody)
      .subscribe((res) => {
        console.log(res)
        this.toastr.success('Success!');
      })

    }

    // if (this.checkboxes.every(val => val.checked == true))
    // this.checkboxes.forEach(val => { val.checked = false });
    // else
    // this.checkboxes.forEach(val => { val.checked = true });
  }
  
  loadUserList(url: string){
    this.userService.fetchUserList(url)
    .subscribe((res) => {
      this.dtOptionsAssign = {
        pagingType: 'full_numbers',
        pageLength: 5,         
        destroy: true,
        retrieve:true,
        paging: false        
      };
      this.userList = res.body      
      console.log( 'user list ' + JSON.stringify(res.body))
      for(let key of this.userList){
        this.tempCheckList.push({'userId': key.userId})
      }
      this.isLoad = false
      this.extractData
      this.dtTriggerAssign.next();
    },
    (error) => {
      this.isLoad = false
      this.noDataFound = 'No data found!'
    })       
  }

  private extractData(res: Response) {
    const body = res.body;
    return body || {};
  }

}
