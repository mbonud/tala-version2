import { Component, OnInit } from '@angular/core';
import { Subject, ReplaySubject } from 'rxjs';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { NgForm, FormControl } from '@angular/forms';
import { RankService } from '@app/shared/services/rank.service'
import { PrincipalService } from '@app/shared/services/principal.service'
import { LibraryService } from '@app/shared/services/library.service';
import { Courses, Course, CourseBody } from '@app/models/course.model';
import { Ranksss } from '@app/models/rank.model';
import { FlashCardBody } from '@app/models/flash-card.model';
import { QuizBody } from '@app/models/quizzes.model';
import { INgxSelectOption } from 'ngx-select-ex';

@Component({
  selector: 'app-edit-modal',
  templateUrl: './edit-modal.component.html',
  styleUrls: ['./edit-modal.component.css']
})
export class EditModalComponent implements OnInit {
  public selectPrincipal = new FormControl();
  public selectRank = new FormControl();
  public selectType = new FormControl();
  public selectContent = new FormControl();
  public seletedContentType: INgxSelectOption[];
  rankList: []
  data:  []
  rankId = [];
  groupId;
  group;
  principalName;
  principalId;
  typeId;
  title: string  
  itemType: string;
  itemId: string;
  contentType;
  acceptedFormat = 'Accepted Format: ';
  itemDetails;
  presignedUrl = 'presigned/';
  chooseFile = "Choose File"
  imgUrl = 'no file chosen...'
  fileUrl;
  videoUrl;
  imgThumbnail;
  itemThumbnailName;
  itemActualFileName;
  itemPreviewFileName;
  presignedThumbnail;
  presignedActualVideo;
  presignedPreviewVideo;
  withThumbnail = false
  withFile = false
  format;  
  thumbnailImageFormat = '3';
  actualVideoFileFormat = '3';
  previewVideoFileFormat = '3';
  thumbnailResponse = 0;
  actualVideoResponse = 0;
  previewVideoResponse = 0;
  fileSize;  
  defaultRankListValue = [];
  thumbnailUrl;
  previewFileUrl;
  courseBody = {
    'courseName': '',
    'courseDescription': '',
    rankId: [],
    'courseType': '',
    'groupId': '',
    // 'courseId': '',    
    'courseThumbnail': '',
    'courseFileName': '',	
    'courseFileSize': '',
    'preview': '',
    'contentType': '',
    'needPayment': '', 
    // 'groupName': '',		  
  };
  flashcardBody = {
    'cardName': '',
    'cardDescription': '',
    rankId: [],
    'cardType': '',
    'groupId': '',
    'cardThumbnail': '',
    'cardFileName': '',	
    'cardFileSize': '',    
    'contentType': '',
    'needPayment': '', 
    // 'groupName': '',
  };
  quizBody = {
    'quizName': '',
    'quizDescription': '',
    rankId: [],
    'quizType': '',
    'groupId': '',
    'quizThumbnail': '',
    'quizFileName': '',	
    'quizFileSize': '',    
    'contentType': '',
    'needPayment': '', 
    // 'groupName': '',
  };
  
  contentList;
  
  editItemDetails = {
    'itemName': '',
    'description': '',
    rankId: [],
    'typeName': '',
    'groupId': '',
    'id': '',
    'thumbnail': '',
    'fileName': '',
    'fileSize': '',
    'preview': '',
    'contentType': '',
    'needPayment': '',    
    'groupName': '',
  }

  status: string
  public onClose: Subject<boolean>;
  public active: boolean = false;  
  stats =  [    
    { typeName: 'Registered', typeId: 'registered' },
    { typeName: 'Group', typeId: 'group' },
    { typeName: 'Free', typeId: 'free' }
  ]  
  principalList = []
  
  constructor(
    public bsModalRef: BsModalRef,
    private rankService: RankService,
    private principalService: PrincipalService,
    private libraryService: LibraryService
  ) { 
    this.selectRank.valueChanges.subscribe(value => this.rankId = value)
    this.selectPrincipal.valueChanges.subscribe(value => this.groupId = value)
    this.selectType.valueChanges.subscribe(value => this.typeId = value)
    this.selectContent.valueChanges.subscribe(value => this.contentType = value)
  }

  ngOnInit(): void {
    this.itemType = this.data['itemType']
    if(this.itemType == 'courses'){
      this.contentList = [
        { contentType: 'VIDEO', contentId: 'VIDEO', contentFormat: '.mp4 / .avi' },
        { contentType: 'PDF', contentId: 'PDF', contentFormat: '.pdf' },  
      ]
    } else if(this.itemType == 'flashcards'){      
      this.contentList = [            
        { contentType: 'TEXT ONLY', contentId: 'CSV', contentFormat: '.csv' }
      ]
    } else if(this.itemType == 'quizzes'){      
      this.contentList = [            
        { contentType: 'MULTIPLE CHOICE', contentId: 'CSV', contentFormat: '.csv'}
      ]
    }
    this.presignedUrl = this.presignedUrl + this.itemType
    this.itemId = this.data['itemId']
    this.status = this.data['status']
    this.principalName = this.data['groupName']
    this.principalId = this.data['groupId']
    console.log(this.principalName)
    // this.group = JSON.parse(JSON.stringify(this.data['group']))
   
    // this.imgThumbnail = '/assets/defaultImage.png';
    // this.videoUrl = '/assets/videoImg.png';        
    this.onClose = new Subject();
    this.fetchRankList();
    this.principalService.fetchPrincialList('groups')
    .subscribe((res) => {
        this.principalList = JSON.parse(JSON.stringify(res.body.groups))        
    })

    const url = this.itemType + '/' + this.itemId
    
      if(this.itemType == 'courses'){
        this.libraryService.fetchCourseDetails(url)
          .subscribe((res) => {
          this.itemDetails = res.body
          this.videoUrl = this.itemDetails['preview']
          console.log('video url ' + this.itemDetails['preview'])
          this.editItemDetails = this.itemDetails
          console.log(JSON.stringify(this.itemDetails))
          this.previewFileUrl = this.itemDetails['preview'].replace(/%20/g, " ").split('/').pop()
          this.editItemDetails.description = this.itemDetails['courseDescription']
          this.editItemDetails.fileName = this.itemDetails['courseFileName']
          this.editItemDetails.itemName = this.itemDetails['courseName']
          this.editItemDetails.thumbnail = this.itemDetails['courseThumbnail']
          this.thumbnailUrl = this.itemDetails['courseUrl']
          this.editItemDetails.typeName = this.itemDetails['courseType']
          // this.editItemDetails.preview = this.itemDetails['preview']
          console.log(this.itemDetails['preview'].replace(/%20/g, " ").split('/').pop())
          this.editItemDetails.preview =  this.itemDetails['preview'].replace(/%20/g, " ").split('/').pop()
          this.editItemDetails.id = this.itemDetails['courseId']        
          // this.imgUrl = this.truncateChar(this.editItemDetails['thumbnail'])
          this.imgUrl =  this.editItemDetails['thumbnail'].split('/').pop() 
          this.fileUrl = this.truncateChar(this.editItemDetails['fileName'])
          
          this.contentType = this.itemDetails['contentType']
          this.imgThumbnail = this.itemDetails['courseThumbnail']
          this.editItemDetails.contentType = this.itemDetails['contentType']
          this.editItemDetails.rankId = this.itemDetails['rankId']
          this.editItemDetails.groupId = this.itemDetails['groupId']
          this.editItemDetails.groupName = this.itemDetails['groupName']
          this.editItemDetails.needPayment = this.itemDetails['needPayment']  
        })
      } else if(this.itemType == 'flashcards'){
        this.libraryService.fetchFlashCardDetails(url)
          .subscribe((res) =>{
            this.itemDetails = res.body
            this.editItemDetails = this.itemDetails
            console.log(JSON.stringify(this.itemDetails))
            this.editItemDetails.description = this.itemDetails['cardDescription']
            this.editItemDetails.fileName = this.itemDetails['cardFileName']
            this.editItemDetails.itemName = this.itemDetails['cardName']
            this.editItemDetails.thumbnail = this.itemDetails['cardThumbnail'].replace(/%20/g, " ").split('/').pop()
            this.thumbnailUrl = this.itemDetails['cardUrl']
            this.editItemDetails.typeName = this.itemDetails['cardType']
            this.editItemDetails.id = this.itemDetails['cardId']
            this.imgUrl = this.truncateChar(this.editItemDetails['thumbnail'])
            this.fileUrl = this.truncateChar(this.editItemDetails['fileName'])
            this.contentType = this.itemDetails['contentType']
            this.imgThumbnail = this.itemDetails['cardThumbnail']
          })        
      } else if(this.itemType == 'quizzes'){
        console.log('this url ' + url)
        this.libraryService.fetchQuizDetails(url)
          .subscribe((res) =>{
            this.itemDetails = res.body
            this.editItemDetails = this.itemDetails
            console.log(JSON.stringify(this.itemDetails))
            this.editItemDetails.description = this.itemDetails['quizDescription']
            this.editItemDetails.fileName = this.itemDetails['quizFileName']
            this.editItemDetails.itemName = this.itemDetails['quizName']
            this.editItemDetails.thumbnail = this.itemDetails['quizThumbnail'].replace(/%20/g, " ").split('/').pop()
            this.thumbnailUrl = this.itemDetails['quizUrl']
            this.editItemDetails.typeName = this.itemDetails['quizType']
            this.editItemDetails.id = this.itemDetails['quizId']
            this.imgUrl = this.truncateChar(this.editItemDetails['thumbnail'])
            this.fileUrl = this.truncateChar(this.editItemDetails['fileName'])
            this.contentType = this.itemDetails['contentType']
            this.imgThumbnail = this.itemDetails['quizThumbnail']
          })           
      }
     
    
  }
  
  onSubmit(formValue : NgForm): void {
    
    let groupName = this.principalList.find(x => x.groupId === this.groupId);  
    // console.log('by group ' + JSON.stringify(groupName.groupName))
    if(this.itemType == 'courses'){
      this.courseBody.courseName = formValue.value.itemName
      this.courseBody.courseDescription = formValue.value.description
      this.courseBody.rankId = this.rankId
      this.courseBody.courseType = this.typeId
      this.courseBody.groupId = this.groupId      
      this.courseBody.courseThumbnail = this.imgUrl
      this.courseBody.courseFileName = this.fileUrl
      this.courseBody.preview = this.previewFileUrl
      this.courseBody.contentType = this.contentType
      this.courseBody.needPayment = formValue.value.payment.toString()
      this.courseBody.courseFileSize = this.fileSize
      console.log('gourp id ' + this.groupId )
      const data: CourseBody = this.courseBody
      console.log('this data ' + JSON.stringify(this.courseBody))
      this.libraryService.updateCourseData(data, this.itemDetails['courseId'])
        .subscribe((res) =>{
          console.log(res)
          this.active = false;
          this.onClose.next(true);
          this.bsModalRef.hide();
        },
        (error) => {
          
        }
      )
    } else if(this.itemType == 'flashcards'){
      this.flashcardBody.cardName = formValue.value.itemName
      this.flashcardBody.cardDescription = formValue.value.description
      this.flashcardBody.rankId = this.rankId
      this.flashcardBody.cardType = this.typeId
      this.flashcardBody.groupId = this.groupId      
      this.flashcardBody.cardThumbnail = this.imgUrl
      this.flashcardBody.cardFileName = this.fileUrl      
      this.flashcardBody.contentType = this.contentType
      this.flashcardBody.needPayment = formValue.value.payment.toString()
      this.flashcardBody.cardFileSize = this.fileSize
      
      const data: FlashCardBody = this.flashcardBody
      console.log('this data ' + JSON.stringify(this.flashcardBody))
      this.libraryService.updateFlashcardData(data, this.itemDetails['cardId'])
        .subscribe((res) =>{
          console.log(res)
          this.active = false;
          this.onClose.next(true);
          this.bsModalRef.hide();
        },
        (error) => {
          
        }
      )    
    } else if(this.itemType == 'quizzes'){
      this.quizBody.quizName = formValue.value.itemName
      this.quizBody.quizDescription = formValue.value.description
      this.quizBody.rankId = this.rankId
      this.quizBody.quizType = this.typeId
      this.quizBody.groupId = this.groupId      
      this.quizBody.quizThumbnail = this.imgUrl
      this.quizBody.quizFileName = this.fileUrl      
      this.quizBody.contentType = this.contentType
      this.quizBody.needPayment = formValue.value.payment.toString()
      this.quizBody.quizFileSize = this.fileSize
      
      const data: QuizBody = this.quizBody      
      this.libraryService.updateQuizData(data, this.itemDetails['quizId'])
        .subscribe((res) =>{
          console.log(res)
          this.active = false;
          this.onClose.next(true);
          this.bsModalRef.hide();
        },
        (error) => {
          
        }
      )
    }
    
    // console.log(this.courseBody)
  }


  fetchRankList(){
    this.rankService.fetchRankList('ranks?limit=100')
    .subscribe(
      (res)=> {        
        this.rankList = res.body.ranks                
        // console.log('this.ranklist ' + JSON.stringify(this.rankList))
      })
  }

  doSelectionContentType(options: INgxSelectOption[]) {
    this.acceptedFormat = "Accepted Format: "
    this.seletedContentType = options;
    let contentFormat = JSON.parse(JSON.stringify(this.contentList.find(x => x.contentId === options[0].value)))    
    this.acceptedFormat = this.acceptedFormat + contentFormat.contentFormat

    this.fileUrl = 'Select ' + contentFormat.contentId + ' file.'
    this.actualVideoFileFormat = '0'
  }

  public onCancel(): void {
    console.log('cancel')
    this.active = false;
    this.onClose.next(false);
    this.bsModalRef.hide();
  }
 
  truncateChar(text: string): string {
    let charlimit = 50;
    if(!text || text.length <= charlimit ){
        return text;
    }

    let without_html = text.replace(/<(?:.|\n)*?>/gm, '');
    let shortened = without_html.substring(0, charlimit) + "...";
    return shortened;
  }

 //upload img
 uploadImg(file){
  this.imgThumbnail = 'C:\\fakepath\\' + file[0].name
  if (file.length === 0)
    return;

  var mimeType = file[0].type;
  if (mimeType.match(/image\/*/) == null) {      
    this.imgUrl = 'Only images with format of .jpg and .png are supported'
    this.thumbnailImageFormat = '0'
    return;
  }

  var reader = new FileReader();    
  reader.readAsDataURL(file[0]); 
  reader.onload = (_event) => { 
    this.presignedThumbnail = file
    this.imgUrl = this.truncateChar(file[0].name)
    this.itemThumbnailName = file[0].name
    this.thumbnailImageFormat = '1'
    this.imgThumbnail = reader.result;
  }    
}

uploadPresignedFile(file: string){
  let fileToUpload;
  let fileName;
  let fileType;
  if(file == 'thumbnail'){
    fileName = this.presignedThumbnail[0].name
    fileType = this.presignedThumbnail[0].type
    fileToUpload = this.presignedThumbnail[0]
  } else if(file == 'actualVideo'){
    fileName = this.presignedActualVideo.name
    fileType = this.presignedActualVideo.type
    fileToUpload = this.presignedActualVideo
  } else if(file == 'previewVideo'){
    fileName = this.presignedPreviewVideo.name
    fileType = this.presignedPreviewVideo.type
    fileToUpload = this.presignedPreviewVideo
  }
  
  let fileUpload =  fileName + ":" + fileType
  console.log(this.presignedThumbnail)
  this.libraryService.uploadFile(fileUpload, this.presignedUrl)
    .subscribe((res) => {
      let generatedUrl = res.body
      console.log(generatedUrl.url)
      this.libraryService.putFile(generatedUrl.url, fileToUpload, fileType)
        .subscribe((res) => {
          if(file == 'thumbnail'){
            this.thumbnailResponse = res['message']
          } else if(file == 'actualVideo'){
            this.actualVideoResponse = res['message']
          } else if(file == 'previewVideo'){
            this.previewVideoResponse = res['message']
          }          
        },
        (error) => {          
      })
    },
    (error) => {
      // console.log('errr' + JSON.stringify(error))
      if(file == 'thumbnail'){
        this.imgUrl = 'File name already exist.'
        this.thumbnailImageFormat = '0'
      } else if(file == 'actualVideo'){
        this.fileUrl = 'File name already exist.'
        this.actualVideoFileFormat = '0'
      } else if(file == 'previewVideo'){
        this.previewFileUrl = 'File name already exist.'
        this.previewVideoFileFormat = '0'
      }
    }
  )
}

uploadActualFile(event){
  const file = event.target.files && event.target.files[0];
  this.fileSize = this.handleSize(file.size)
  console.log(file)    
  if (file) {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    this.mimeTypeChecker(this.contentType, file.type, file.name, file)
    // if(file.type.indexOf('csv')> -1){
    //   console.log('this is csv file')
    //   this.fileUrl = 'Only Video with format of .mp4 and .avi are supported'
    //   this.actualVideoFileFormat = '0'
    // } else if(file.type.indexOf('image')> -1){
    //   this.fileUrl = 'Only Video with format of .mp4 and .avi are supported'
    //   this.actualVideoFileFormat = '0'        
    // } else if(file.type.indexOf('video')> -1){
    //   this.fileUrl = this.truncateChar(file.name)
    //   this.presignedActualVideo = file
    //   this.actualVideoFileFormat = '1'
    // }
    reader.onload = (event) => {
      // this.videoUrl = (<FileReader>event.target).result;
    }
    console.log(file.type)
  }
}

private mimeTypeChecker(file: string, fileType: string, fileName: string, actualFile: any){
  let csv = ['application/vnd.ms-excel','text/csv','text/tsv']
  let image = ['image/jpeg','image/jpg','images/jpeg', 'images/jpg', 'image/png', 'images/png']
  let video = ['video/mp4','video/avi']
  let txt = ['text/plain','text/*']
  let pdf = ['application/pdf']
  let check: boolean = false;
  console.log('content type ' + file )
  
  if(file == 'CSV'){
    if(csv.indexOf(fileType) !== -1){
      check = true;
      this.fileUrl = this.truncateChar(fileName)
      this.itemActualFileName = fileName
      this.presignedActualVideo = actualFile
      this.actualVideoFileFormat = '1'
    } else {
      this.fileUrl = 'Only Csv with format of .csv is supported'
      this.actualVideoFileFormat = '0'
    }
  } else if(file == 'IMAGE'){
    if(image.indexOf(fileType) !== -1){ check = true }
    this.itemThumbnailName = fileName
  } else if(file == 'VIDEO'){
    if(video.indexOf(fileType) !== -1){
       check = true;
       this.itemActualFileName = fileName
       this.fileUrl = this.truncateChar(fileName)
       this.presignedActualVideo = actualFile
       this.actualVideoFileFormat = '1'
    } else {
      this.fileUrl = 'Only Video with format of .mp4 and .avi are supported'
      this.actualVideoFileFormat = '0'
    }
  } else if(file == 'TEXT ONLY'){
    if(csv.indexOf(fileType) !== -1){
      check = true;
      this.fileUrl = this.truncateChar(fileName)
      this.itemActualFileName = fileName
      this.presignedActualVideo = actualFile
      this.actualVideoFileFormat = '1'
    } else {
      this.fileUrl = 'Only CSV with format of .csv are supported'
      this.actualVideoFileFormat = '0'
    }
  } else if(file == 'PDF'){
    if(pdf.indexOf(fileType) !== -1){
       check = true;
       this.itemActualFileName = fileName
       this.fileUrl = this.truncateChar(fileName)
       this.presignedActualVideo = actualFile
       this.actualVideoFileFormat = '1'
    } else {
      this.fileUrl = 'Only PDF with format of .pdf are supported'
      this.actualVideoFileFormat = '0'
    }
  } else if(file == 'MULTIPLE CHOICE'){
    if(csv.indexOf(fileType) !== -1){
       check = true;
       this.itemActualFileName = fileName
       this.fileUrl = this.truncateChar(fileName)
       this.presignedActualVideo = actualFile
       this.actualVideoFileFormat = '1'
    } else {
      this.fileUrl = 'Only CSV with format of .csv are supported'
      this.actualVideoFileFormat = '0'
    }
  }  
  }

uploadPreviewFile(event){
  const file = event.target.files && event.target.files[0];

  if (file) {
    var reader = new FileReader();
    reader.readAsDataURL(file);
    if(file.type.indexOf('image')> -1){
      this.previewFileUrl = 'Only Video with format of .mp4 and .avi are supported'
      this.previewVideoFileFormat = '0'
    } else if(file.type.indexOf('video')> -1){
      this.previewFileUrl = this.truncateChar(file.name)
      this.itemPreviewFileName = file.name
      this.presignedPreviewVideo = file
      this.previewVideoFileFormat = '1'
    }
    reader.onload = (event) => {
      this.videoUrl = (<FileReader>event.target).result;
    }
  }
}

handleSize(size){
  let unit;
  if (size < 1000) {
    size = size;
    unit = "bytes";
  } else if (size < 1000*1000) {
    size = size / 1000;
    unit = "kb";
  } else if (size < 1000*1000*1000) {
    size = size / 1000 / 1000;
    unit = "mb";
  } else {
    size = size / 1000 /1000 /1000;
    unit = "gb";
  }
  return parseFloat(size).toFixed(2) + ' ' + unit
  console.log(parseFloat(size).toFixed(2) + ' ' + unit)
}
}
