import { Component, OnInit } from '@angular/core';
import { Subject, ReplaySubject } from 'rxjs';
import { NgForm, FormControl, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { RankService } from '@app/shared/services/rank.service'
import { PrincipalService } from '@app/shared/services/principal.service'
import { LibraryService } from '@app/shared/services/library.service';
import { Course, CourseBody } from '@app/models/course.model'
import { FlashCard, FlashCardBody } from '@app/models/flash-card.model';
import { QuizBody } from '@app/models/quizzes.model';
import { INgxSelectOption } from 'ngx-select-ex';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-add-modal',
  templateUrl: './add-modal.component.html',
  styleUrls: ['./add-modal.component.css']
})
export class AddModalComponent implements OnInit {
  public selectPrincipal = new FormControl(null, [Validators.required]);
  public selectRank = new FormControl(null, [Validators.required]);
  public selectType = new FormControl(null, [Validators.required]);
  public selectStatus = new FormControl(null, [Validators.required]);
  public selectContent = new FormControl(null, [Validators.required]);
  public seletedContentType: INgxSelectOption[];
  presignedUrl = 'presigned/';
  rankList: []
  data:  []    
  title: string  
  itemType: string;  
  itemDetails;  
  chooseFile = "Choose File"
  imgUrl;
  fileUrl;
  itemThumbnailName;
  itemActualFileName;
  itemPreviewFileName;
  previewFileUrl;
  format;
  acceptedFormat = 'Accepted Format: ';
  videoUrl;
  imgThumbnail;
  presignedThumbnail;
  presignedActualVideo;
  presignedPreviewVideo;
  defaultRankListValue = [];
  rankId = [];
  contentDefault;
  groupId;
  typeId;
  uniqId;
  statusId;
  status: string
  thumbnailImageFormat = '3';
  actualVideoFileFormat = '3';
  previewVideoFileFormat = '3';
  fileSize;  
  public onClose: Subject<boolean>;
  public active: boolean = false;
  stats =  [
    { typeName: 'Registered', typeId: 'registered' },
    { typeName: 'Group', typeId: 'group' },
    { typeName: 'Free', typeId: 'free' }
  ]
  statusType =  [
    { statusName: 'Active', statusId: 'active' },
    { statusName: 'Inactive', statusId: 'inactive' },
    { statusName: 'Special', statusId: 'special' }
  ]
  contentList;
  principalList = []
  contentType;
  courseBody = {
    'courseName': '',
    'courseDescription': '',
    rankId: [],
    'courseType': '',
    'groupId': '',
    // 'courseId': '',    
    'courseThumbnail': '',
    'courseFileName': '',	
    'courseFileSize': '',
    'preview': '',
    'contentType': '',
    'needPayment': '', 
    // 'groupName': '',
  };

  flashcardBody = {
    'cardName': '',
    'cardDescription': '',
    rankId: [],
    'cardType': '',
    'groupId': '',
    'cardThumbnail': '',
    'cardFileName': '',	
    'cardFileSize': '',
    'preview': '',
    'contentType': '',
    'needPayment': '', 
    // 'groupName': '',
  };

  quizBody = {
    'quizName': '',
    'quizDescription': '',
    rankId: [],
    'quizType': '',
    'groupId': '',
    'quizThumbnail': '',
    'quizFileName': '',	
    'quizFileSize': '',    
    'contentType': '',
    'needPayment': '', 
    // 'groupName': '',
  };
  
  thumbnailResponse = 0;
  actualVideoResponse = 0;
  previewVideoResponse = 0;
  model: any = {}
  constructor(
    public bsModalRef: BsModalRef,
    private rankService: RankService,
    private principalService: PrincipalService,
    private libraryService: LibraryService,
    private toastr: ToastrService
  ) { 
    this.selectRank.valueChanges.subscribe(value => this.rankId = value)
    this.selectPrincipal.valueChanges.subscribe(value => this.groupId = value)
    this.selectType.valueChanges.subscribe(value => this.typeId = value)
    this.selectStatus.valueChanges.subscribe(value => this.statusId = value)
    this.selectContent.valueChanges.subscribe(value =>  this.contentType = value)
  }

  ngOnInit(): void {    
    this.itemType = this.data['itemType']
    if(this.itemType == 'courses'){      
      this.contentList = [
        { contentType: 'VIDEO', contentId: 'VIDEO', contentFormat: '.mp4 / .avi' },
        { contentType: 'PDF', contentId: 'PDF', contentFormat: '.pdf' },  
      ]
    } else if(this.itemType == 'flashcards'){
      this.contentList = [            
        { contentType: 'TEXT ONLY', contentId: 'CSV', contentFormat: '.csv' }
      ]
    } else if(this.itemType == 'quizzes'){
      this.contentList = [            
        { contentType: 'MULTIPLE CHOICE', contentId: 'CSV', contentFormat: '.csv'}
      ]
    }
    this.contentDefault = this.contentList[0].contentId
    console.log(this.contentDefault)
    this.acceptedFormat = this.acceptedFormat + this.contentList[0].contentFormat    
    this.presignedUrl = this.presignedUrl + this.itemType
    console.log(this.presignedUrl)
    this.imgThumbnail = '/assets/defaultImage.png';
    this.videoUrl = '/assets/videoImg.png';    
    this.onClose = new Subject();
    this.fileUrl = 'Select content type first to enable this upload.'
    this.fetchRankList();
    this.fetchPrincipalList()
  }

  fetchPrincipalList(){
    this.principalService.fetchPrincialList('groups')
    .subscribe((res) => {
        this.principalList = JSON.parse(JSON.stringify(res.body.groups))
    })  
  }

  fetchRankList(){
    this.rankService.fetchRankList('ranks?limit=100')
    .subscribe(
      (res)=> {        
        this.rankList = res.body.ranks                
        // console.log('this.ranklist ' + JSON.stringify(this.rankList))
      })
  }
  
  doSelectionContentType(options: INgxSelectOption[]) {
    this.acceptedFormat = "Accepted Format: "
    this.seletedContentType = options;
    let contentFormat = JSON.parse(JSON.stringify(this.contentList.find(x => x.contentId === options[0].value)))    
    this.acceptedFormat = this.acceptedFormat + contentFormat.contentFormat

    this.fileUrl = 'Select ' + contentFormat.contentId + ' file.'
    this.actualVideoFileFormat = '0'
  }

  public onCancel(): void {
    this.active = false;
    this.onClose.next(false);
    this.bsModalRef.hide();
  }

  public onConfirm(): void {
    this.active = false;
    this.onClose.next(false);
    this.bsModalRef.hide();
  }

  onSubmit(formValue : NgForm): void { 
    let groupName = this.principalList.find(x => x.groupId === this.groupId);  
    // console.log('by group ' + JSONstringify(groupName.groupName))
    if(this.itemType == 'courses'){
      this.courseBody.courseName = formValue.value.itemName
      this.courseBody.courseDescription = formValue.value.description
      this.courseBody.rankId = this.rankId
      this.courseBody.courseType = this.typeId
      // this.courseBody.status = this.statusId
      this.courseBody.groupId = this.groupId      
      this.courseBody.courseThumbnail = this.itemThumbnailName
      this.courseBody.courseFileName = this.itemActualFileName
      this.courseBody.preview = this.itemPreviewFileName
      this.courseBody.contentType = this.contentType
      this.courseBody.needPayment = formValue.value.payment.toString()
      this.courseBody.courseFileSize = this.fileSize
      const data: CourseBody = this.courseBody
        this.libraryService.uploadCourseData(data)
          .subscribe((res) =>{
            console.log(res)
            this.active = false;
            this.onClose.next(true);
            this.bsModalRef.hide();
          },
          (error) => {
            console.log(error)
        })
    } else if(this.itemType == 'flashcards'){
      this.flashcardBody.cardName = formValue.value.itemName
      this.flashcardBody.cardDescription = formValue.value.description
      this.flashcardBody.rankId = this.rankId
      this.flashcardBody.cardType = this.typeId
      this.flashcardBody.groupId = this.groupId      
      this.flashcardBody.cardThumbnail = this.itemThumbnailName
      this.flashcardBody.cardFileName = this.itemActualFileName      
      this.flashcardBody.contentType = this.contentType
      this.flashcardBody.needPayment = formValue.value.payment.toString()
      this.flashcardBody.cardFileSize = this.fileSize
      delete this.flashcardBody['preview']
      console.log(this.flashcardBody)
      const data: FlashCardBody = this.flashcardBody
      this.libraryService.uploadFlashcardData(data)
      .subscribe((res) =>{
        console.log(res)
        this.active = false;
        this.onClose.next(true);
        this.bsModalRef.hide();
      },
      (error) => {
        console.log(error)
      })
    } else if(this.itemType == 'quizzes'){
      this.quizBody.quizName = formValue.value.itemName
      this.quizBody.quizDescription = formValue.value.description
      this.quizBody.rankId = this.rankId
      this.quizBody.quizType = this.typeId
      this.quizBody.groupId = this.groupId      
      this.quizBody.quizThumbnail = this.itemThumbnailName
      this.quizBody.quizFileName = this.itemActualFileName      
      this.quizBody.contentType = this.contentType
      this.quizBody.needPayment = formValue.value.payment.toString()
      this.quizBody.quizFileSize = this.fileSize
      console.log(this.quizBody)
      delete this.quizBody['preview']
      console.log(this.quizBody)
      const data: QuizBody = this.quizBody
      this.libraryService.uploadQuizData(data)
      .subscribe((res) =>{
        console.log(res)
        this.active = false;
        this.onClose.next(true);
        this.bsModalRef.hide();
      },
      (error) => {
        
      })
    }
        
    
    
    // console.log(this.courseBody)
  }

  truncateChar(text: string): string {
    let charlimit = 50;
    if(!text || text.length <= charlimit ){
        return text;
    }

    let without_html = text.replace(/<(?:.|\n)*?>/gm, '');
    let shortened = without_html.substring(0, charlimit) + "...";
    return shortened;
  }

  //upload img
  uploadImg(file){
    this.imgThumbnail = 'C:\\fakepath\\' + file[0].name
    if (file.length === 0)
      return;
 
    var mimeType = file[0].type;
    if (mimeType.match(/image\/*/) == null) {      
      this.imgUrl = 'Only images with format of .jpg and .png are supported'
      this.thumbnailImageFormat = '0'
      return;
    }
 
    var reader = new FileReader();    
    reader.readAsDataURL(file[0]); 
    reader.onload = (_event) => { 
      this.presignedThumbnail = file
      this.imgUrl = this.truncateChar(file[0].name)
      this.itemThumbnailName = file[0].name
      this.thumbnailImageFormat = '1'
      this.imgThumbnail = reader.result;
    }    
  }

  uploadPresignedFile(file: string){
    let fileToUpload;
    let fileName;
    let fileType;
    console.log('fiel ' + file)
    if(file == 'thumbnail'){
      fileName = this.presignedThumbnail[0].name
      fileType = this.presignedThumbnail[0].type
      fileToUpload = this.presignedThumbnail[0]
    } else if(file == 'actualVideo'){
      fileName = this.presignedActualVideo.name
      fileType = this.presignedActualVideo.type
      fileToUpload = this.presignedActualVideo
    } else if(file == 'previewVideo'){
      fileName = this.presignedPreviewVideo.name
      fileType = this.presignedPreviewVideo.type
      fileToUpload = this.presignedPreviewVideo
    }
    console.log('fileName ' + fileName)
    console.log('fileType ' + fileType)
    console.log('fileToUpload ' + JSON.stringify(fileToUpload))
    console.log('presignedUrl ' + this.presignedUrl)
    let fileUpload =  fileName + ":" + fileType
    console.log('fileUpload ' + fileUpload)
    this.libraryService.uploadFile('btslogo.png:image/png', 'api/presigned/courses')
    console.log(this.presignedThumbnail)
    this.libraryService.uploadFile(fileUpload, this.presignedUrl)
      .subscribe((res) => {
        let generatedUrl = res.body
        console.log(generatedUrl.url)
        this.libraryService.putFile(generatedUrl.url, fileToUpload, fileType)
          .subscribe((res) => {            
            if(file == 'thumbnail'){
              this.thumbnailResponse = res['message']
            } else if(file == 'actualVideo'){
              console.log('message ' + res['message'])
              this.actualVideoResponse = res['message']              
            } else if(file == 'previewVideo'){
              console.log('preview message ' + res['message'])
              this.previewVideoResponse = res['message']
            }
            if(res['message'] == 100){
              this.toastr.success('Success!');
            }  
          },
          (error) => {
            this.toastr.error('File name already exist!');
          }
          )
      },
      (error) => {
        if(file == 'thumbnail'){
          this.imgUrl = 'File name already exist.'
          this.thumbnailImageFormat = '0'
        } else if(file == 'actualVideo'){
          this.fileUrl = 'File name already exist.'
          this.actualVideoFileFormat = '0'
        } else if(file == 'previewVideo'){
          this.previewFileUrl = 'File name already exist.'
          this.previewVideoFileFormat = '0'
        }
        this.toastr.error('File name already exist!');
      }
    )
  }

  uploadActualFile(event){
    const file = event.target.files && event.target.files[0];
    this.fileSize = this.handleSize(file.size)
    console.log(file)    
    if (file) {
      var reader = new FileReader();
      reader.readAsDataURL(file);
      this.mimeTypeChecker(this.contentType, file.type, file.name,file)
      // if(file.type.indexOf('csv')> -1){
      //   console.log('this is csv file')
      //   this.fileUrl = 'Only Video with format of .mp4 and .avi are supported'
      //   this.actualVideoFileFormat = '0'
      // } else if(file.type.indexOf('image')> -1){
      //   this.fileUrl = 'Only Video with format of .mp4 and .avi are supported'
      //   this.actualVideoFileFormat = '0'        
      // } else if(file.type.indexOf('video')> -1){
      //   this.fileUrl = this.truncateChar(file.name)
      //   this.presignedActualVideo = file
      //   this.actualVideoFileFormat = '1'
      // }
      this.presignedActualVideo = file
      reader.onload = (event) => {
        // this.videoUrl = (<FileReader>event.target).result;
      }
      console.log(file.type)
    }
  }

  private mimeTypeChecker(file: string, fileType: string, fileName: string, actualFile: any){
    let csv = ['application/vnd.ms-excel','text/csv','text/tsv']
    let image = ['image/jpeg','image/jpg','images/jpeg', 'images/jpg', 'image/png', 'images/png']
    let video = ['video/mp4','video/avi']
    let txt = ['text/plain','text/*']
    let pdf = ['application/pdf']
    let check: boolean = false;
    console.log('content type ' + file )
    
    if(file == 'CSV'){
      if(csv.indexOf(fileType) !== -1){
        check = true;
        this.fileUrl = this.truncateChar(fileName)
        this.itemActualFileName = fileName
        this.presignedActualVideo = actualFile
        this.actualVideoFileFormat = '1'
      } else {
        this.fileUrl = 'Only Csv with format of .csv is supported'
        this.actualVideoFileFormat = '0'
      }
    } else if(file == 'IMAGE'){
      if(image.indexOf(fileType) !== -1){ check = true }
      this.itemThumbnailName = fileName
    } else if(file == 'VIDEO'){
      if(video.indexOf(fileType) !== -1){
         check = true;
         this.itemActualFileName = fileName
         this.fileUrl = this.truncateChar(fileName)
         this.presignedActualVideo = actualFile
         this.actualVideoFileFormat = '1'
      } else {
        this.fileUrl = 'Only Video with format of .mp4 and .avi are supported'
        this.actualVideoFileFormat = '0'
      }
    } else if(file == 'TEXT ONLY'){
      if(csv.indexOf(fileType) !== -1){
        check = true;
        this.fileUrl = this.truncateChar(fileName)
        this.itemActualFileName = fileName
        this.presignedActualVideo = actualFile
        this.actualVideoFileFormat = '1'
      } else {
        this.fileUrl = 'Only CSV with format of .csv are supported'
        this.actualVideoFileFormat = '0'
      }
    } else if(file == 'PDF'){
      if(pdf.indexOf(fileType) !== -1){
         check = true;
         this.itemActualFileName = fileName
         this.fileUrl = this.truncateChar(fileName)
         this.presignedActualVideo = actualFile
         this.actualVideoFileFormat = '1'
      } else {
        this.fileUrl = 'Only PDF with format of .pdf are supported'
        this.actualVideoFileFormat = '0'
      }
    } else if(file == 'MULTIPLE CHOICE'){
      if(csv.indexOf(fileType) !== -1){
         check = true;
         this.itemActualFileName = fileName
         this.fileUrl = this.truncateChar(fileName)
         this.presignedActualVideo = actualFile
         this.actualVideoFileFormat = '1'
      } else {
        this.fileUrl = 'Only CSV with format of .csv are supported'
        this.actualVideoFileFormat = '0'
      }
    }
    
  }

  uploadPreviewFile(event){
    const file = event.target.files && event.target.files[0];

    if (file) {
      var reader = new FileReader();
      reader.readAsDataURL(file);
      if(file.type.indexOf('image')> -1){
        this.previewFileUrl = 'Only Video with format of .mp4 and .avi are supported'
        this.previewVideoFileFormat = '0'
      } else if(file.type.indexOf('video')> -1){
        this.previewFileUrl = this.truncateChar(file.name)
        this.itemPreviewFileName = file.name
        this.presignedPreviewVideo = file
        this.previewVideoFileFormat = '1'
      }
      reader.onload = (event) => {
        this.videoUrl = (<FileReader>event.target).result;
      }
    }
  }

  handleSize(size){
    let unit;
    if (size < 1000) {
      size = size;
      unit = "bytes";
    } else if (size < 1000*1000) {
      size = size / 1000;
      unit = "kb";
    } else if (size < 1000*1000*1000) {
      size = size / 1000 / 1000;
      unit = "mb";
    } else {
      size = size / 1000 /1000 /1000;
      unit = "gb";
    }
    return parseFloat(size).toFixed(2) + ' ' + unit
    console.log(parseFloat(size).toFixed(2) + ' ' + unit)
  }
}
