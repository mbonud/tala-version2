import { Component, OnInit, Input, EventEmitter, Output } from '@angular/core';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { Course } from '@app/models/course.model';
import { FlashCard } from '@app/models/flash-card.model';
import { Quizzes } from '@app/models/quizzes.model';
import { ModalService } from '@app/shared/modal/modal.service'
import { LibraryService } from '@app/shared/services/library.service'
import { Subject } from 'rxjs';
import { CourseBody } from '@app/models/course.model'

@Component({
  selector: 'app-delete-modal',
  templateUrl: './delete-modal.component.html',
  styleUrls: ['./delete-modal.component.css']
})
export class DeleteModalComponent implements OnInit {
  data:  []
  title: string  
  itemType: string;
  itemId: string;
  status: string
  public onClose: Subject<boolean>;
  public active: boolean = false;  
  itemCourseData: Course;
	itemCardData: FlashCard;
  itemQuizData: Quizzes;
  itemData: any;
  
  constructor(
    public bsModalRef: BsModalRef,
    private modalService: ModalService,
    private libraryService: LibraryService
  ) { }

  ngOnInit(): void {
    this.itemType = this.data['itemType']
    this.itemId = this.data['itemId']
    this.status = this.data['status']
    this.onClose = new Subject();
    console.log(this.data['status'])
    const url = `${this.itemType}/${this.itemId}`    
    console.log(url)
    this.libraryService.fetchLibaryDetails(`${this.itemType}/${this.itemId}`)
    .subscribe(
      (res) => {
        if(this.itemType == 'courses'){
          this.itemCourseData = res.body;
          this.itemData = this.itemCourseData
        } else if(this.itemType == 'flashcards'){
          this.itemCardData = res.body;
          this.itemData = this.itemCardData
        } else if(this.itemType == 'quizzes'){
          this.itemQuizData = res.body;
          this.itemData = this.itemQuizData
          console.log(res.body)
        }        
      })
  }

public onCancel(): void {
    this.active = false;
    this.onClose.next(false);
    this.bsModalRef.hide();
}
  onConfirm(){    
    console.log(this.itemType);
    console.log(this.itemId);
    console.log(this.status)
       
    console.log('status ' + this.status)
    if(this.status == 'active'){
      console.log('active')
      this.libraryService.disableData(this.itemType, this.itemId)
      .subscribe((res) => {
        this.active = false;
        this.onClose.next(true);
        this.bsModalRef.hide();
      })
    } else if(this.status == 'inactive'){
      this.itemData.status = 'active'
      console.log('inactive ' + JSON.stringify(this.itemData))
			this.libraryService.enableData(this.itemType, this.itemData, this.itemId)
        .subscribe(
          (res) => {
            this.active = false;
            this.onClose.next(true);
            this.bsModalRef.hide();
          },
        );
    }
  }

}
