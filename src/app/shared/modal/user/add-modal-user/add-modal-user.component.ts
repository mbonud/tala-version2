import { Component, OnInit } from '@angular/core';
import { Subject, ReplaySubject } from 'rxjs';
import { NgForm, FormControl, Validators } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { RankService } from '@app/shared/services/rank.service'
import { PrincipalService } from '@app/shared/services/principal.service'
import { UserService } from '@app/shared/services/user.service'
import { ToastrService } from 'ngx-toastr';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// export interface UserDetails {
//   firstName: string;
//   middleName: string;
//   lastName: string;
//   gender: string;
//   crewNo: string;
//   birthday: string;
//   mobile: string;
//   group: string;
//   rankId: string;
//   userType: string;
//   email: string;
//   password: string;
// }

@Component({
  selector: 'app-add-modal-user',
  templateUrl: './add-modal-user.component.html',
  styleUrls: ['./add-modal-user.component.css']
})
export class AddModalUserComponent implements OnInit {
  public selectPrincipal = new FormControl(null, [Validators.required]);
  public selectRank = new FormControl(null, [Validators.required]);
  public selectUserType = new FormControl(null, [Validators.required]);
  public selectGender = new FormControl(null, [Validators.required]);
  userDetails;
  loader = false;
  // userType =  [    
  //   { typeName: 'SUPER ADMIN  ', typeId: 'superAdmin' },
  //   // { typeName: 'GROUP ADMIN', typeId: 'groupAdmin' },
  //   // { typeName: 'GROUP USER', typeId: 'groupUser' },
  //   { typeName: 'TRAINEE', typeId: 'trainee' }
  // ]
  userType;
  rankId;
  groupId;
  rankList: []
  principalList = []
  gender;
  model: any = {};
  public onClose: Subject<boolean>;
  public active: boolean = false;

  constructor(
    public bsModalRef: BsModalRef,
    private rankService: RankService,
    private principalService: PrincipalService,
    private userService: UserService,
    private toastr: ToastrService
  ) { 
    this.selectRank.valueChanges.subscribe(value => this.rankId = value)
    this.selectPrincipal.valueChanges.subscribe(value => this.groupId = value)
    this.selectUserType.valueChanges.subscribe(value => this.userType = value)
    this.selectGender.valueChanges.subscribe(value => this.gender = value)
  }

  ngOnInit(): void {
    this.onClose = new Subject();
    // this.selectGender.valueChanges
    //   .subscribe((subscriptionTypeId: number) => {
    //     // this.model.gender
    //     console.log(
    //       'subscriptionTypeId', subscriptionTypeId
    //     );
    //   });
    this.fetchRankList();
    this.fetchPrincipalList()
  }

  fetchPrincipalList(){
    this.principalService.fetchPrincialList('groups')
    .subscribe((res) => {
        this.principalList = JSON.parse(JSON.stringify(res.body.groups))
    })  
  }

  fetchRankList(){
    this.rankService.fetchRankList('ranks?limit=100')
    .subscribe(
      (res)=> {        
        this.rankList = res.body.ranks                
        // console.log('this.ranklist ' + JSON.stringify(this.rankList))
      })
  }

  public onCancel(): void {
    this.active = false;
    this.onClose.next(false);
    this.bsModalRef.hide();
  }

  public onSubmit(formValue: NgForm): void {    
    this.loader = true
    console.log(this.userType)
    console.log(formValue.value.firstName)    
    let bday = new Date(formValue.value.birthday);  
    // bday = 
    console.log()
    this.userDetails = {
      firstName: formValue.value.firstName,
      middleName: formValue.value.middleName,
      lastName: formValue.value.lastName,
      gender: this.gender,
      crewNo: formValue.value.crewNo,
      birthday: bday.toLocaleDateString(),    
      mobile: formValue.value.mobile.toString(),
      groupId: this.groupId,
      rankId: this.rankId,
      userType: this.userType,
      email: formValue.value.email,
      password: formValue.value.password,            
    }
    console.log(this.userDetails)
    this.userService.addNewUser(this.userDetails)
      .subscribe((res) => {
        this.loader = false
        this.active = false;
        this.onClose.next(true);
        this.bsModalRef.hide();
      },
      (error) => {
        this.loader = false
        console.log(error)
        console.log(error.error.errorMessage)
        console.log(JSON.stringify(error))
        // this.toastr.success('Success!');
        this.toastr.error(JSON.stringify(Object.values(error.error)[0]));
      })  
  }

}
