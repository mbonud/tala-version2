import { Component, OnInit } from '@angular/core';
import { Subject, ReplaySubject } from 'rxjs';
import { NgForm, FormControl } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { RankService } from '@app/shared/services/rank.service'
import { PrincipalService } from '@app/shared/services/principal.service'
import { UserService } from '@app/shared/services/user.service'
import { DatePipe } from '@angular/common';
import { ToastrService } from 'ngx-toastr';

// export interface  UserDetails  {
//     firstName: string,
//     middleName: string,
//     lastName: string,
//     birthday: string,
//     gender: string,
//     userType: string,
//     crewNo: string,
//     mobile: string,
//     groupId: string,
//     rank: string,
//     rankId: string,
//     email: string,
//   };

@Component({
  selector: 'app-edit-modal-user',
  templateUrl: './edit-modal-user.component.html',
  styleUrls: ['./edit-modal-user.component.css']
})
export class EditModalUserComponent implements OnInit {
  public selectPrincipal = new FormControl();
  public selectRank = new FormControl();
  public selectUserType = new FormControl();
  public selectGender = new FormControl();  
  title;
  bday;
  typeName;  
  // userDetails;
  userBody;
  userDetails = {
    firstName: '',
    middleName: '',
    lastName: '',
    birthday: '',
    gender: '',
    userType: '',
    crewNo: '',
    mobile: '',
    groupId: '',
    rank: '',
    rankId: '',
    email:''
  };
  userTypeList =  [    
    { userTypeName: 'SUPER ADMIN  ', userTypeId: 'superAdmin' },    
    { userTypeName: 'TRAINEE', userTypeId: 'trainee' }
  ]
  userType;
  rankId;
  groupId;  
  rankList: []
  principalList = []
  gender;
  public onClose: Subject<boolean>;
  public active: boolean = false;
  userId;

  constructor(
    private datePipe: DatePipe,
    public bsModalRef: BsModalRef,
    private rankService: RankService,
    private principalService: PrincipalService,
    private userService: UserService,
    private toastr: ToastrService
  ) { 
    this.selectRank.valueChanges.subscribe(value => this.rankId = value)
    this.selectPrincipal.valueChanges.subscribe(value => this.groupId = value)
    this.selectUserType.valueChanges.subscribe(value => this.userType = value)
    this.selectGender.valueChanges.subscribe(value => this.gender = value)
  }

  ngOnInit(): void {
    this.onClose = new Subject();
    this.fetchRankList();
    this.fetchPrincipalList()
    this.fetchUserData(this.userId)
  }

  fetchUserData(userId: string){
    console.log(userId)
    this.userService.fetchUserData(userId)
    .subscribe((res) => {
      
      let bday: Date = new Date(res.body.birthday)
      this.bday = this.formatDate(res.body.birthday)
      console.log(this.datePipe.transform(this.bday, 'yyyy-mm-dd'))
      // this.bday=new Date();
      // let latest_date = 
      this.userDetails = res.body
      // this.userDetails.birthday = this.datePipe.transform(bday, 'yyyy-mm-dd');
      // this.userType = res.body['userType']
      this.typeName = 'TRAINEE'
      console.log('res1 ' + this.typeName)
      console.log('res2 ' + res.body['userType'])
      // console.log('res3 ' + JSON.parse(this.userDetails))
      console.log('res4 ' + JSON.parse( JSON.stringify(this.userDetails)))
      // this.userDetails.birthday = bday
      console.log('res5 ' + JSON.stringify(this.userDetails))
      // this.userDetails = JSON.stringify(this.userDetails)
      // console.log(JSON.parse( JSON.stringify(this.userDetails)))
    })
  }

  formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) 
        month = '0' + month;
    if (day.length < 2) 
        day = '0' + day;
        let dates = [year, month, day].join('-')
    console.log(dates)
    return [year, month, day].join('-');
}

  fetchPrincipalList(){
    this.principalService.fetchPrincialList('groups')
    .subscribe((res) => {
        this.principalList = JSON.parse(JSON.stringify(res.body.groups))
    })  
  }

  fetchRankList(){
    this.rankService.fetchRankList('ranks?limit=100')
    .subscribe(
      (res)=> {        
        this.rankList = res.body.ranks                
        // console.log('this.ranklist ' + JSON.stringify(this.rankList))
      })
  }

  public onCancel(): void {
    this.active = false;
    this.onClose.next(false);
    this.bsModalRef.hide();
  }

  public onSubmit(formValue: NgForm): void {
    console.log(formValue.value.firstName)    
    let bday = new Date(formValue.value.birthday);  
    // bday = 
    console.log()
    this.userBody = {
      firstName: formValue.value.firstName,
      middleName: formValue.value.middleName,
      lastName: formValue.value.lastName,
      gender: this.gender,
      crewNo: formValue.value.crewNo,
      birthday: bday.toLocaleDateString(),
      // mobile: formValue.value.mobile,
      mobile: formValue.value.mobile.toString(),
      groupId: this.groupId,
      rankId: this.rankId,
      userType: this.userType,
      email: formValue.value.email,             
    }
    console.log(this.userDetails)
    this.userService.updateUserData(this.userBody, this.userId)
      .subscribe((res) => {        
        // this.toastr.success('Success!');        
        this.active = false;
        this.onClose.next(true);
        this.bsModalRef.hide();
      }) 
  }
}
