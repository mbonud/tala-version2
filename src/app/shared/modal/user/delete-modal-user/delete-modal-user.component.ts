import { Component, OnInit } from '@angular/core';
import { Subject, ReplaySubject } from 'rxjs';
import { NgForm, FormControl } from '@angular/forms';
import { BsModalRef } from 'ngx-bootstrap/modal';
import { UserService } from '@app/shared/services/user.service';

@Component({
  selector: 'app-delete-modal-user',
  templateUrl: './delete-modal-user.component.html',
  styleUrls: ['./delete-modal-user.component.css']
})
export class DeleteModalUserComponent implements OnInit {
  public onClose: Subject<boolean>;
  public active: boolean = false;
  userId;
  title;

  constructor(
    public bsModalRef: BsModalRef,        
    private userService: UserService
  ) { }

  ngOnInit(): void {
    this.onClose = new Subject();
  }

  public onCancel(): void {
    this.active = false;
    this.onClose.next(false);
    this.bsModalRef.hide();
  }

  onConfirm(): void{
    this.userService.deleteUserData(this.userId)
    .subscribe((res) =>{
      console.log(res)
      this.active = false;
      this.onClose.next(true);
      this.bsModalRef.hide();
    })
  }
}
