import { Component, OnInit, OnDestroy, ViewEncapsulation, Inject} from '@angular/core';
import { NgForm } from '@angular/forms';
import { User } from '../../models/user.model';
import { Router, ActivatedRoute } from '@angular/router';
import { AuthService } from 'src/app/core/auth/auth.service';
import { DOCUMENT } from '@angular/common';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css'],
  encapsulation: ViewEncapsulation.None,
})
export class LoginComponent implements OnInit {
  showHidePassword: boolean
  isLoad = false
  password = "Btsolve2019|"
  email = "support@btsolve.com"

  constructor(
    @Inject(DOCUMENT) private _document,
    private authService: AuthService,
    private router: Router,
    private route: ActivatedRoute,
    private toastr: ToastrService
  ) { }

  ngOnInit(): void {
    this._document.body.classList.add('bodybg-color');
  }

  ngOnDestroy() {
    // remove the class form body tag
    this._document.body.classList.remove('bodybg-color');    
  }

  toggleFshowHidePassword(){
    this.showHidePassword = !this.showHidePassword;
    console.log(this.showHidePassword)
  }

  onSubmit(form: NgForm){  
    let email = form.value.email
    let password = form.value.password
    console.log(form.value)
    this.authService.login(email, password)
      .subscribe((res) => {
        let user: User = res.body
        if(user.userType != null){        
          this.router.navigateByUrl('/dashboard/seafarer')
        }
        console.log(user)
      },
      (error) => {
        this.toastr.error('Failed to Sign in!');
      }
    )
  }

}
