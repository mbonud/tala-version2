import { BrowserModule } from '@angular/platform-browser';
import { NgModule, Component } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { DatePipe } from '@angular/common';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { CoreModule } from './core/core.module';

import { PageNotFoundComponent } from './page-not-found/page-not-found.component';
import { AdminComponent } from './pages/admin/admin.component';
import { StudentComponent } from './pages/student/student.component';
import { AdminModule } from './pages/admin/admin.module';
import { LibraryModule } from './pages/admin/library/library.module';
import { LayoutComponent } from './pages/admin/admin-layout/layout/layout.component';
import { HeaderComponent } from './pages/admin/admin-layout/header/header.component';
import { DataTablesModule } from 'angular-datatables';
import { HttpClientModule } from '@angular/common/http';
import { map } from 'rxjs/operators';
import { ModalModule, BsModalRef } from 'ngx-bootstrap/modal';
import { TooltipModule } from 'ngx-bootstrap/tooltip';
import { NgxSelectModule } from 'ngx-select-ex';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { ToastrModule, ToastContainerModule } from 'ngx-toastr';
import { TabsModule } from 'ngx-bootstrap/tabs';
// modal component library
import { AddModalComponent } from '@app/shared/modal/library/add-modal/add-modal.component';
import { EditModalComponent } from '@app/shared/modal/library/edit-modal/edit-modal.component';
import { DeleteModalComponent } from '@app/shared/modal/library/delete-modal/delete-modal.component'
import { AssignModalComponent } from './shared/modal/library/assign-modal/assign-modal.component';
import { AssigningModalComponent } from './shared/modal/library/assign-modal/assigning-modal/assigning-modal.component';
// modal component group
import { AddModalGroupComponent } from './shared/modal/group/add-modal-group/add-modal-group.component';
import { EditModalGroupComponent } from './shared/modal/group/edit-modal-group/edit-modal-group.component';
import { DeleteModalGroupComponent } from './shared/modal/group/delete-modal-group/delete-modal-group.component';
// modal component psychology
import { AddModalPsychologyComponent } from './shared/modal/psychology/add-modal-psychology/add-modal-psychology.component';
import { EditModalPsychologyComponent } from './shared/modal/psychology/edit-modal-psychology/edit-modal-psychology.component';
// modal component rank
import { AddModalRankComponent } from './shared/modal/rank/add-modal-rank/add-modal-rank.component';
import { EditModalRankComponent } from './shared/modal/rank/edit-modal-rank/edit-modal-rank.component';
import { DeleteModalRankComponent } from './shared/modal/rank/delete-modal-rank/delete-modal-rank.component';
// modal component user
import { AddModalUserComponent } from './shared/modal/user/add-modal-user/add-modal-user.component';
import { EditModalUserComponent } from './shared/modal/user/edit-modal-user/edit-modal-user.component';
import { DeleteModalUserComponent } from './shared/modal/user/delete-modal-user/delete-modal-user.component';
import { MustMatchDirective } from './_helpers/must-match.directive';
import { DashboardModule } from './pages/admin/dashboard/dashboard.module';
import { DashboardCourseModalComponent } from './shared/modal/dashboard/dashboard-course-modal/dashboard-course-modal.component';
import { RatingModule } from 'ng-starrating';
import { DashboardQuizModalComponent } from './shared/modal/dashboard/dashboard-quiz-modal/dashboard-quiz-modal.component';
import { DashboardFlashcardModalComponent } from './shared/modal/dashboard/dashboard-flashcard-modal/dashboard-flashcard-modal.component';
import { AuthGuard } from './core/auth/auth.guard';
import { AuthService } from './core/auth/auth.service';
import { AuthGuardGuard } from './auth-guard.guard';


@NgModule({
  declarations: [
    AppComponent,        
    PageNotFoundComponent,  
    AdminComponent,
    StudentComponent,
    LayoutComponent,
    HeaderComponent,    
    EditModalComponent,
    DeleteModalComponent,    
    AddModalComponent, AddModalGroupComponent, EditModalGroupComponent, 
    DeleteModalGroupComponent, AddModalPsychologyComponent, EditModalPsychologyComponent, 
    AddModalRankComponent, EditModalRankComponent, DeleteModalRankComponent, 
    AddModalUserComponent, EditModalUserComponent, DeleteModalUserComponent, 
    AssignModalComponent,
    AssigningModalComponent,
    MustMatchDirective,
    DashboardCourseModalComponent,
    DashboardQuizModalComponent,
    DashboardFlashcardModalComponent,    
  ],
  imports: [
    BrowserModule,
    RatingModule ,
    FormsModule,
    ReactiveFormsModule,
    AppRoutingModule,    
    CoreModule,    
    AdminModule,
    LibraryModule,  
    DashboardModule,  
    DataTablesModule,
    HttpClientModule,
    ModalModule.forRoot(),
    TooltipModule.forRoot(),
    NgxSelectModule,
    BrowserAnimationsModule, 
    ToastrModule.forRoot(),
    ToastContainerModule,
    TabsModule.forRoot(),    

  ],
  entryComponents: [  
    AddModalComponent,
    EditModalComponent,
    DeleteModalComponent,
    AssignModalComponent,
    AssigningModalComponent,
    AddModalGroupComponent,
    EditModalGroupComponent,
    DeleteModalGroupComponent,
    AddModalPsychologyComponent,
    EditModalPsychologyComponent,
    AddModalRankComponent,
    EditModalRankComponent,
    DeleteModalRankComponent,
    AddModalUserComponent,
    EditModalUserComponent,
    DeleteModalUserComponent,
    DashboardCourseModalComponent,
    DashboardQuizModalComponent
  ],
  providers: [BsModalRef, DatePipe, AuthGuardGuard, AuthService],
  bootstrap: [AppComponent]
})
export class AppModule { }
