import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, map } from 'rxjs/operators';

const SESSION_KEY = 'tala-session';

@Injectable()
export class SessionService {

	storage: Storage;
	
	constructor(private httpClient: HttpClient) {
		this.storage = window.localStorage;
		// const storage = this.getFromStorage();
		// if (!storage) {
		// 	this.setToStorage({
		// 		ttl: 3600
		// 	});
		// }
	}
	
	getValue(key: string): any {
		const storage = this.getFromStorage();
		return storage && storage[key] ? storage[key] : null;
	}

	setValue(key: string, value: any): void {
		const storage = this.getFromStorage();
		const data = storage ? Object.assign(storage, { [key]: value }) : { [key]: value };
		this.setToStorage(data);
	}

	forget(key: string): void {
		const storage = this.getFromStorage();
		if (storage && storage[key]) {
			delete storage[key];
			this.setToStorage(storage);
		}
	}

	flush(): void {
		this.storage.removeItem(SESSION_KEY);
	}

	private getFromStorage(): any {
		const data = this.storage.getItem(SESSION_KEY) ? atob(this.storage.getItem(SESSION_KEY)) : null;
		return data ? JSON.parse(data) : null;
	}

	private setToStorage(data): void {
		this.storage.setItem(SESSION_KEY, btoa(JSON.stringify(data)));
	}

	public refreshToken(rToken: string): Observable<HttpResponse<any>>{
        return this.httpClient
        .post<any>(`oauth/token`, {refreshToken : rToken}, { observe: 'response' })
        .pipe(
            catchError((error: HttpErrorResponse) => throwError(error))
        );
    }

}
