import { Injectable } from '@angular/core';
import { HttpClient, HttpResponse, HttpErrorResponse, HttpHeaders } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError, switchMap, tap } from 'rxjs/operators';
import { User } from '@app/models/user.model';
import { Tokens } from '@app/models/tokens.model';
import { SessionService } from '../session/session.service';
import { AuthGuard } from './auth.guard';

@Injectable()
export class AuthService {

	constructor(
		private httpClient: HttpClient,
		private sessionService: SessionService,
		private authGuard: AuthGuard
	) { }	

	login(username: string, password: string): Observable<HttpResponse<User>> {
		return this.getAccessToken(username, password)
			.pipe(
				switchMap((response: HttpResponse<Tokens>) => this.getUser(response.body))
			);
	}

	logout() {
		this.sessionService.flush();
		console.log('logout')
	}

	private getUser(tokens: Tokens): Observable<HttpResponse<User>> {
		// console.log(tokens);
		return this.httpClient		
			// .get<User>(`auth/users/${tokens.userId}`, {
				.get<User>(`users/${tokens.userId}`, {
				headers: new HttpHeaders({ 'Authorization': `Bearer ${tokens.accessToken}` }),
				observe: 'response'
			})
			.pipe(
				tap((response: HttpResponse<User>) => this.sessionService.setValue('user', response.body)),
				catchError((error: HttpErrorResponse) => throwError(error))
			);
	}

	isLogin(){
		return true;
	}

	private getAccessToken(username: string, password: any): Observable<HttpResponse<Tokens>> {
		const basicAuth = window.btoa(`${username}:${password}`);
		return this.httpClient
			.post<Tokens>(`users/login`, {}, {
				headers: new HttpHeaders({ 'Authorization': `Basic ${basicAuth}` }),
				observe: 'response'
			})
			.pipe(
				tap((response) => this.sessionService.setValue('tokens', response.body)),
				catchError((error: HttpErrorResponse) => throwError(error))
			);
	}

	

	isAuthenticated(): boolean {
		return !!(this.sessionService.getValue('tokens') && this.sessionService.getValue('user'));
	}

	get tokens(): Tokens | null {
		return this.sessionService.getValue('tokens');
	}

	get user(): User | null {
		return this.sessionService.getValue('user');
	}
}
