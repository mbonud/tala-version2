import { NgModule, Optional, SkipSelf } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClient, HttpClientModule } from '@angular/common/http';
// import { ToastrModule } from 'ngx-toastr';

import { AuthService } from './auth/auth.service';
import { HttpService } from './http/http.service';
import { ApiPrefixInterceptor } from './http/interceptors/api-prefix.interceptor';
import { ErrorHandlerInterceptor } from './http/interceptors/error-handler.interceptor';
import { AuthInterceptor } from './http/interceptors/auth.interceptor';
import { SessionService } from './session/session.service';
import { RefreshTokenInterceptor } from './http/interceptors/refresh-token.interceptor';

@NgModule({
	imports: [
		CommonModule,
		HttpClientModule,
		// ToastrModule.forRoot()
	],
	declarations: [],
	providers: [
		SessionService,
		AuthService,
		AuthInterceptor,
		ApiPrefixInterceptor,		
		ErrorHandlerInterceptor,
		RefreshTokenInterceptor,
		{
			provide: HttpClient,
			useClass: HttpService
		}
	]
})
export class CoreModule {
	constructor(@Optional() @SkipSelf() parentModule: CoreModule) {
		if (parentModule) {
			throw new Error(`${parentModule} has already been loaded. Import Core module in the AppModule only.`);
		}
	}
}
