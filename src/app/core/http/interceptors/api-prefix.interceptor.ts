import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Constants } from '@app/core/utils';

@Injectable()
export class ApiPrefixInterceptor implements HttpInterceptor {

	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		request = request.clone({ url: Constants.serverBaseUrl + request.url });
		return next.handle(request);
	}

}
