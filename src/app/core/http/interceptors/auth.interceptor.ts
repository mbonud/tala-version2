import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthService } from '@app/core/auth/auth.service';
import { Tokens } from '@app/models/tokens.model';


@Injectable()
export class AuthInterceptor implements HttpInterceptor {

	constructor(
		private authService: AuthService
	) { }

	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		if (this.authService.isAuthenticated()) {
			const tokens: Tokens = this.authService.tokens;
			request = request.clone({
				headers: request.headers.set('Authorization', `Bearer ${tokens.accessToken}`)
			});
		}
		return next.handle(request);
			
	}

	

}
