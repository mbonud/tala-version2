import { Injectable, Injector } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse, HttpResponse ,HttpClient} from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '@environments/environment';
import { AuthService } from '@app/core/auth/auth.service';
import { AuthInterceptor } from './auth.interceptor';
import { SessionService } from '@app/core/session/session.service';
import { Tokens } from '@app/models/tokens.model';
import { Response } from 'selenium-webdriver/http';
import { Constants } from '@app/core/utils';
import { Router } from '@angular/router';
import { tap } from 'rxjs/operators';

@Injectable()
export class RefreshTokenInterceptor implements HttpInterceptor {

    constructor(
        private injector: Injector,
        
    private router: Router,
	) {   }

    intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    
        return next.handle(request).pipe(tap((event: HttpEvent<any>) => {
          if (event instanceof HttpResponse) {
          }
        }, (err: any) => {
          if (err instanceof HttpErrorResponse) {
              if (err.url === Constants.serverBaseUrl + 'users/login'){
                    return err;
              } else {
                    if (err.status === 401) {
                            console.warn('Request error', err);
                            let sessionService = this.injector.get(SessionService);
                            const data = atob(sessionService.storage.getItem('tala-session'));
                            const s = JSON.parse(data);
                            const token : Tokens = s.tokens;
                            return sessionService.refreshToken(token.refreshToken).subscribe(
                                (response) => {
                                    sessionService.setValue('tokens', Object.assign( response.body, {userId : s.user.userId}));
                                    return next.handle(request)
                                },
                                (error) => {
                                  this.router.navigateByUrl('/accounts/logout');
                                  console.log(error)
                                
                                }
                            );
                    } 
                }
          }
        }));
      }
      
}