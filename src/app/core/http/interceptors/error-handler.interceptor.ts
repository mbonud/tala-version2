import { Injectable } from '@angular/core';
import { HttpEvent, HttpInterceptor, HttpHandler, HttpRequest, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { catchError } from 'rxjs/operators';
import { environment } from '@environments/environment';

@Injectable()
export class ErrorHandlerInterceptor implements HttpInterceptor {

	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
		return next.handle(request)
			.pipe(
				catchError((error: HttpEvent<any>) => this.errorHandler(error))
			);
	}

	private errorHandler(response: HttpEvent<any>): Observable<HttpEvent<any>> {
		if (!environment.production) {
			if (response instanceof HttpErrorResponse) {
				if ((<HttpErrorResponse>response).status !== 304) {
					console.warn('Request error', response);
				}
			}
		}
		throw response;
	}
}
