export class Constants {
	static serverBaseUrl = `http://talalms.ap-northeast-1.elasticbeanstalk.com/api/v1/`
	// LIVE	
	// static serverBaseUrl = `http://tala-maritime.ap-northeast-1.elasticbeanstalk.com/api/v1/`;	
	
	// Dev	
	// static serverBaseUrl = `http://tala-dev3-server.ap-northeast-1.elasticbeanstalk.com/api/v1/`;	

	// Staging 
	// static serverBaseUrl = `http://tala-server-demo.ap-northeast-1.elasticbeanstalk.com/api/v1/`;
	}

export class Helpers {
	static generatePrevSet(url, uriName): string | null {
		if (url && uriName) {
			const regexp = new RegExp(uriName + '-\\d+', 'g');
			const matched = url.match(regexp);
			const uri = matched && matched.length > 0 ? matched.shift() : null;
			if (uri) {
				const splitted = uri.split('-');
				const pageNumber = splitted && splitted.length === 2 ? Number(splitted[1]) : 0;
				if (pageNumber > 1) {
					return url.replace(regexp, (match) => {
						const [name, page] = match.split('-');
						return `${name}-${Number(page) - 1}`;
					});
				}
			}
		}
		return null;
	}
}
